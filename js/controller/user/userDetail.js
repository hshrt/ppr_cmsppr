App.UserDetail = Backbone.View.extend({

    // el - stands for element. Every view has a element associate in with HTML content will be rendered.
    el: '#content_container',
    // It's the first function called when this view it's instantiated.
    parentId: 0,
    jsonObj: null,
    title: "Guest",
    initialize: function(options){
        if(options && options.parentId){
            this.parentId = options.parentId;
        }
        if(options && options.listTitle){
            this.title = options.listTitle;
        }
        this.render();

    },
    events: {
        'click #closeBtn_start' : 'backToIndex'
    },
    // $el - it's a cached jQuery object (el), in which you can use jQuery functions to push content. Like the Hello World in this case.
    render: function(){
        self = this;

        //console.log("should set: " + App.currentItem.salutation +". "+ App.currentItem.lastName);


        setTimeout(function(){App.subHead.setPathText(-1,App.currentItem.firstName +". "+ App.currentItem.lastName);}, 500);


        $.ajax({
            url : "php/html/user/userDetail.php",
            method : "POST",
            dataType: "html",
            data : { }
        }).success(function(html){
            $(self.el).append(html).
                promise()
                .done(function(){
                    //self.loadPhoto(self.page);
                    console.log("halo");
                    $("#itemName").text(App.currentItem.firstName +". "+ App.currentItem.lastName);


                    $("#delete_btn").text("Delete User");
                    $("#save_btn").text("Save User");


                    console.log(App.currentItem.salutation);

                    $("#firstNameInput").val(App.currentItem.firstName);
                    $("#lastNameInput").val(App.currentItem.lastName);
                    $("#emailInput").val(App.currentItem.email);
                    $("#roleInput").val(App.currentItem.role);

                    if(App.currentItem.role == "admin"){
                        $('#roleInput option')[0].selected = true;
                    }
                    else if(App.currentItem.role == "editor"){
                        $('#roleInput option')[1].selected = true;
                    }


                    $("#delete_btn").on("click",function(){


                        App.yesNoPopup = new App.YesNoPopup(
                            {
                                yesFunc:function()
                                {
                                    $.ajax({
                                        url : "api/user/deleteUser.php",
                                        method : "POST",
                                        dataType: "json",
                                        data : {userId:App.currentItem.id }//canParking: UserData.canParking, canInvite: UserData.canInvite , ticket : UserData.Ticket
                                    }).success(function(json){

                                        console.log(json);

                                        if(json.status == 502){
                                            alert("Session is time-out, please login again.");
                                            location.reload();
                                            return;
                                        }
                                        //self.yesFunc();
                                        App.yesNoPopup.destroy();
                                        if(json.status == 1){

                                            App.goUpperLevel();
                                        }
                                        else{
                                            alert("Delete Fail. Error occur");
                                        }

                                    }).error(function(d){
                                        console.log('error');
                                        console.log(d);
                                    });
                                },
                                msg:"Are you sure to delete this item?"
                            }
                        );
                    });

                    $("#save_btn").on("click",function(){


                        $.ajax({
                            url : "api/user/updateUser.php",
                            method : "POST",
                            dataType: "json",
                            data : {userId:App.currentItem.id,
                                    firstName:$("#firstNameInput").val() ,
                                    lastName:$("#lastNameInput").val() ,
                                    email:$("#emailInput").val() ,
                                    role:$("#roleInput").val()
                            }
                        }).success(function(json){

                            console.log(json);

                            if(json.status == 502){
                                alert(App.strings.sessionTimeOut);
                                location.reload();
                                return;
                            }

                            //self.yesFunc();

                            if(json.status == 1){

                                App.goUpperLevel();
                            }
                            else{
                                alert("Update Fail. Error occur");
                            }

                        }).error(function(d){
                            console.log('error');
                            console.log(d);
                        });
                    });

                });

        }).error(function(d){
            console.log('error');
            console.log(d);
        });


    },
    showPopup: function(){
        var self = this;
        //alert(self.title);
        App.createItemPopup = new App.CreateItemPopup(
            {

                title: self.title == "Library"?"Page":self.title,
                parentId: self.parentId
            }
        );
    },
    backToIndex: function(){
        window.parent.PeriEvent.backToIndex();
    },
    moveToTheme: function(){
        //this.destroy();
        $(this.el).hide();
        $("#blackTemp").show();
        $("#content").show();
    },
    showUp: function(){
        $(this.el).show();
        this.isHide = false;
        $("#blackTemp").hide();
    },
    close :function(){
        console.log("close fire");
    },
    destroy: function() {

        //COMPLETELY UNBIND THE VIEW


        //this.$el.removeData().unbind();

        //Remove view from DOM
        $("#itemListContainer").remove();
        this.undelegateEvents();
        //this.initialize();
        //Backbone.View.prototype.remove.call(this);

    },
    isHide : false
});