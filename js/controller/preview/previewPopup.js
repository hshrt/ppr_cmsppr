App.PreviewPopup = Backbone.View.extend({

    // el - stands for element. Every view has a element associate in with HTML content will be rendered.
    el: '#container',
    itemTitle: "",
    parentTitle: "",
    itemImageLink: "",
    description: "",
    showTypeBox:true,
    parentId: 0,
    // It's the first function called when this view it's instantiated.
    initialize: function(options){
        if(options && options.previewType){
            this.previewType = options.previewType;
        }
        if(options && options.itemTitle){
            this.itemTitle = options.itemTitle;
        }
        if(options && options.parentTitle){
            this.parentTitle = options.parentTitle;
        }
        if(options && options.itemImageLink){
            this.itemImageLink = options.itemImageLink;
        }
        if(options && options.description){
            this.description = options.description;
        }
        if(options && options.price){
            this.price = options.price;
        }
        if(options && options.lang){
            this.lang = options.lang;
        }

        this.render();
    },
    events: {

        'click #cancel_btn'  : 'destroy'
    },
    setupUIHandler : function(){

    },
    // $el - it's a cached jQuery object (el), in which you can use jQuery functions to push content. Like the Hello World in this case.
    render: function(){
        //alert($(window).width());

        var self = this;
        console.log("render createItemPopup");
        $.ajax({
         url : "php/html/preview/previewPopup.php",
         method : "POST",
         dataType: "html",
         data : {}//canParking: UserData.canParking, canInvite: UserData.canInvite , ticket : UserData.Ticket
         }).success(function(html){
             console.log(html);
            $('#container').append(html).
                promise()
                .done(function(){

                    $('.popup_box_container').show(true);

                    var baseImageURL;
                    if(self.previewType == "generalItem"){
                        baseImageURL = "images/general_item_base.png";
                    }
                    else if(self.previewType == "inRoomDinningSubcat"){
                        baseImageURL = "images/in_room_dinning_subcat.png";
                    }
                    else if(self.previewType == "inRoomDinningFood"){
                        baseImageURL = "images/in_room_dinning_food.png";
                    }

                    $("#appBG").css({"background-image":"url('"+baseImageURL+"')"});

                    $("#closeBtn").on('click',function(){
                        console.log("closeBtn clicked");
                        self.destroy();
                    });

                    $("#itemTitleText").text(self.itemTitle);
                    //ellipsis function for the description text
                    $("#itemTitleText").dotdotdot({ellipsis	: '... ',height: 50,wrap: 'word',callback	: function( isTruncated, orgContent ) {if(!isTruncated){console.log("no truncated"); $("#itemTitleText").css("height", "auto");}},watch: "window"});

                    $("#itemImage").css({"background-image":"url('"+self.itemImageLink+"')"});

                    $("#overlayBox").hide();
                    $("#arrowBtn").hide();
                    $("#zoom_container").hide();

                    //hide the greyPopupBox by default
                    $("#greyPopupBox").hide();

                    if(self.previewType == "generalItem") {
                        $("#overlayBox").show();
                        $("#arrowBtn").show();
                        $("#zoom_container").show();

                        $("#description_text").html(self.description);

                        $("#parentTitle").text(self.parentTitle);
                    }
                    else if(self.previewType == "inRoomDinningSubcat") {
                        $("#itemTitle").addClass("subcat");
                        $("#itemImage").addClass("subcat");

                        $("#overlayBox1").addClass("subcat");
                        $("#overlayBox2").addClass("subcat");
                        $("#overlayBox3").addClass("subcat");
                        $("#overlayBox4").addClass("subcat");
                        $("#overlayBox5").addClass("subcat");
                        $("#overlayBox6").addClass("subcat");

                        $("#descriptionOption").hide();
                        $("#desText").hide();
                    }
                    else if(self.previewType == "inRoomDinningFood") {

                        $("#greyPopupBox").show();

                        $("#itemTitle").hide();

                        $("#descriptionOption").hide();
                        $("#desText").hide();

                        $("#imageOption").hide();
                        $("#imageText").hide();

                        $("#foodText").text(self.itemTitle);
                        $("#priceText").text(self.price + " €");

                        $("#title").text(self.itemTitle);
                        $("#greyPopupBox #price").text(self.price + " €");
                        $("#description").html(self.description);

                        $("#overlayBox1,#overlayBox2,#overlayBox3,#overlayBox4,#overlayBox5,#overlayBox6").addClass("foodDetail");
                        $("#overlayBox5,#overlayBox6").hide();

                        $("#greyPopupBox #closeBtn").on('click',function(){
                            $("#greyPopupBox").hide();

                            $("#overlayBox1,#overlayBox2,#overlayBox3,#overlayBox4,#overlayBox5,#overlayBox6").removeClass("foodDetail");
                            $("#overlayBox1,#overlayBox2,#overlayBox3,#overlayBox4,#overlayBox5,#overlayBox6").addClass("food");
                        });

                        $("#foodText,#priceText").on('click',function(){
                            $("#greyPopupBox").show();

                            $("#overlayBox1,#overlayBox2,#overlayBox3,#overlayBox4,#overlayBox5,#overlayBox6").removeClass("food");
                            $("#overlayBox1,#overlayBox2,#overlayBox3,#overlayBox4,#overlayBox5,#overlayBox6").addClass("foodDetail");
                        });

                    }

                    $("#imageOption").change(function() {
                        if(this.checked) {
                            $("#itemImage").show();
                        }
                        else{
                            $("#itemImage").hide();
                        }
                    });

                    $("#descriptionOption").change(function() {
                        if(this.checked) {
                            $("#description_text").show();
                            $("#overlayBox").show();
                            $("#arrowBtn").show();
                            $("#zoom_container").show();
                        }
                        else{
                            $("#description_text").hide();
                            $("#overlayBox").hide();
                            $("#arrowBtn").hide();
                            $("#zoom_container").hide();
                        }
                    });
                    $("#highlightOption").change(function() {
                        if(this.checked) {
                            $("#overlayBox1").show();
                            $("#overlayBox2").show();
                            $("#overlayBox3").show();
                            $("#overlayBox4").show();
                            $("#overlayBox5").show();
                            $("#overlayBox6").show();
                        }
                        else{
                            $("#overlayBox1").hide();
                            $("#overlayBox2").hide();
                            $("#overlayBox3").hide();
                            $("#overlayBox4").hide();
                            $("#overlayBox5").hide();
                            $("#overlayBox6").hide();
                        }
                    });

                    if(self.lang == "ar"){
                        $("#description_text").css("direction", "rtl");
                    }

                    //$("#dimmer").click(function(e){e.preventDefault();});


                });

         }).error(function(d){
            console.log('error');
            console.log(d);
         });
    },

    clickConfirm : function(){

    },

    showUp: function(){
        $(this.el).show();
        this.isHide = false;
        $("#blackTemp").hide();
    },
    close :function(){
        console.log("close fire");
    },
    destroy: function() {

        //COMPLETELY UNBIND THE VIEW
        //this.undelegateEvents();
        //this.$el.removeData().unbind();
        $(".popup_box_container").remove();
        this.undelegateEvents();
        //Backbone.View.prototype.remove.call(this);
        //Remove view from DOM
        //this.remove();
        //Backbone.View.prototype.remove.call(this);

    },
    isHide : false
});