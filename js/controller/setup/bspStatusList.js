App.BspStatusList = Backbone.View.extend({

    // el - stands for element. Every view has a element associate in with HTML content will be rendered.
    el: '#content_container',
    // It's the first function called when this view it's instantiated.
    parentId: 0,
    jsonObjs: null,
    jsonObjs_filtered:null,
    type:null,
    sortType:'bspname',
    sortDirection: 'ASC',
    title: "",
    updateTimeOut:3600000, //60mins
    dictLastUpdateTime:0,
    itemLastUpdateTime:0,
    mediaLastUpdateTime:0,
    initialize: function(options){
        if(options && options.type){
            this.title = this.type = options.type;
        }
        this.render();
    },
    events: {
        'click #closeBtn_start' : 'backToIndex'
    },
    // $el - it's a cached jQuery object (el), in which you can use jQuery functions to push content. Like the Hello World in this case.
    render: function(){

        var self = this;

        $.ajax({
            url : "php/html/itemList.php",
            method : "POST",
            dataType: "html",
            data : { }
        }).success(function(html){

            console.log(html);

            $(self.el).append(html).promise()
                .done(function() {
                    //alert("done");
                    self.postUISetup();
                    $("#boxer").after($("#itemListContainer"));
                });

        }).error(function(d){
            console.log('error');
            console.log(d);
        });

    },
    postUISetup:function(){
        var self = this;

        $("#item_list_head_title").text(upperFirstChar(self.title));
        $('#addBtn').hide();

        App.showLoading();

        // james debug
        var _allUpdateUrl = "api/getAllLastUpdateTime.php";

        $.ajax({
            url : _allUpdateUrl,
            method : "GET",
            dataType: "json",
            data : {}
        }).success(function(json){
            console.log("allUpdateData = " + json.data);
            self.dictLastUpdateTime = json.data[0].dictLastUpdate;
            self.itemLastUpdateTime = json.data[0].itemLastUpdate;
            self.mediaLastUpdateTime = json.data[0].mediaLastUpdate;
            self.drawCommonList(json.data);
        }).error(function(d){
            console.log(d);
        });

        var _url = "api/bspStatus/getAllBspStatus.php";

        $.ajax({
            url : _url,
            method : "GET",
            dataType: "json",
            data : {}
        }).success(function(json){
            //setTimeout(App.hideLoading, 1000);

            App.hideLoading();

            console.log(json);
            self.jsonObjs = json.data;

            self.drawList(self.jsonObjs);

            //self.drawList(self.jsonObjs);
            console.log("jsonObjs is ok");
            console.log(App.idArray);

            $("#bspName_sort_icon").hide();
            $("#lastEvent_sort_icon").hide();
            $("#lastEventTime_sort_icon").hide();
            $("#lastUpdateCheckingTime_sort_icon").hide();
            $("#updateGetLangForKey_sort_icon").hide();
            $("#updateGetItemList_sort_icon").hide();
            $("#updateGetPhoto_sort_icon").hide();
            $("#scheduleUpdate_sort_icon").hide();

        }).error(function(d){
            App.hideLoading();
            console.log('error');
            console.log(d);
        });

        $('#search').on('keyup', function() {
            if (this.value.length > 0) {

                self.jsonObjs_filtered = new Array();
                self.jsonObjs_filtered = searchStringInArrByRoomIdForBspStatus($("#search").val(),self.jsonObjs);

                if(self.jsonObjs_filtered.length == 0){
                    $("#resultTable").empty();

                    $("#noResultMsg").show();

                }
                else{
                    console.log("yeah");

                    $("#noResultMsg").hide();

                    self.filtering = true;

                    self.drawList(self.jsonObjs_filtered);
                }
            }
            else{
                $("#paginationContainer").show();
                $("#noResultMsg").hide();
                self.filtering = false;
                self.drawList(self.jsonObjs);
            }
        });
    },
    sortBy: function(type,order){
        console.log("order = " + order);
        this.sortDirection = order;
        if(type == "lasteventtime"){
            if(order == 'ASC') {
                this.jsonObjs.sort(function (obj1, obj2) {
                    var date1 = obj1.lastUpdate;
                    var date2 = obj2.lastUpdate;
                    if (date1==null) date1="";
                    if (date2==null) date2="";
                    //console.log("obj1: "+date1+" obj2: "+date2);
                    return ((date1 < date2) ? -1 : ((date1 > date2) ? 1 : 0));
                    //return date1 - date2;
                });
            }
            else{
                this.jsonObjs.sort(function (obj1, obj2) {
                    var date1 = obj1.lastUpdate;
                    var date2 = obj2.lastUpdate;
                    if (date1==null) date1="";
                    if (date2==null) date2="";
                    //console.log("obj1: "+date1+" obj2: "+date2);
                    return ((date1 > date2) ? -1 : ((date1 < date2) ? 1 : 0));
                    //return date2 - date1;
                });
            }
        }
        else if(type == "lastevent"){
            if(order == 'ASC') {
                //this.jsonObj.sort(function (obj1, obj2) {
                //return obj1.guestname - obj2.guestname;
                //});

                function SortByName(a, b){
                    var aName = "None";
                    var bName = "None";
                    if (a.event!=null) aName = a.event.toLowerCase();
                    if (b.event!=null) bName = b.event.toLowerCase();

                    return ((aName < bName) ? -1 : ((aName > bName) ? 1 : 0));
                }

                this.jsonObjs.sort(SortByName);

            }
            else{
                function SortByNameRev(a, b){
                    var aName = "None";
                    var bName = "None";
                    if (a.event!=null) aName = a.event.toLowerCase();
                    if (b.event!=null) bName = b.event.toLowerCase();
                    return ((aName > bName) ? -1 : ((aName < bName) ? 1 : 0));
                }

                this.jsonObjs.sort(SortByNameRev);
            }
        }
        else if(type == "bspname"){
            if(order == 'ASC') {
                //this.jsonObj.sort(function (obj1, obj2) {
                //return obj1.guestname - obj2.guestname;
                //});

                function SortByName(a, b){
                    var aName = a.roomName.toLowerCase();
                    var bName = b.roomName.toLowerCase();
                    return ((aName < bName) ? -1 : ((aName > bName) ? 1 : 0));
                }

                this.jsonObjs.sort(SortByName);

            }
            else{
                function SortByNameRev(a, b){
                    var aName = b.roomName.toLowerCase();
                    var bName = a.roomName.toLowerCase();
                    return ((aName < bName) ? -1 : ((aName > bName) ? 1 : 0));
                }

                this.jsonObjs.sort(SortByNameRev);
            }
        }
        else if(type == "lastupdatecheckingtime"){
            if(order == 'ASC') {
                this.jsonObjs.sort(function (obj1, obj2) {
                    var date1 = obj1.updateCheck;
                    var date2 = obj2.updateCheck;
                    if (date1==null) date1="";
                    if (date2==null) date2="";
                    //console.log("obj1: "+date1+" obj2: "+date2);
                    return ((date1 < date2) ? -1 : ((date1 > date2) ? 1 : 0));

                });
            }
            else{
                this.jsonObjs.sort(function (obj1, obj2) {
                    var date1 = obj1.updateCheck;
                    var date2 = obj2.updateCheck;
                    if (date1==null) date1="";
                    if (date2==null) date2="";
                    //console.log("obj1: "+date1+" obj2: "+date2);
                    return ((date1 > date2) ? -1 : ((date1 < date2) ? 1 : 0));

                });
            }
        }
        else if(type == "updateGetLangForKey"){
            if(order == 'ASC') {
                this.jsonObjs.sort(function (obj1, obj2) {
                    var date1 = obj1.updateGetLangForKey;
                    var date2 = obj2.updateGetLangForKey;
                    if (date1==null) date1="";
                    if (date2==null) date2="";
                    //console.log("obj1: "+date1+" obj2: "+date2);
                    return ((date1 < date2) ? -1 : ((date1 > date2) ? 1 : 0));
                });
            }
            else{
                this.jsonObjs.sort(function (obj1, obj2) {
                    var date1 = obj1.updateGetLangForKey;
                    var date2 = obj2.updateGetLangForKey;
                    if (date1==null) date1="";
                    if (date2==null) date2="";
                    //console.log("obj1: "+date1+" obj2: "+date2);
                    return ((date1 > date2) ? -1 : ((date1 < date2) ? 1 : 0));
                });
            }
        }
        else if(type == "updateGetItemList"){
            if(order == 'ASC') {
                this.jsonObjs.sort(function (obj1, obj2) {
                    var date1 = obj1.updateGetItemList;
                    var date2 = obj2.updateGetItemList;
                    if (date1==null) date1="";
                    if (date2==null) date2="";
                    //console.log("obj1: "+date1+" obj2: "+date2);
                    return ((date1 < date2) ? -1 : ((date1 > date2) ? 1 : 0));
                });
            }
            else{
                this.jsonObjs.sort(function (obj1, obj2) {
                    var date1 = obj1.updateGetItemList;
                    var date2 = obj2.updateGetItemList;
                    if (date1==null) date1="";
                    if (date2==null) date2="";
                    //console.log("obj1: "+date1+" obj2: "+date2);
                    return ((date1 > date2) ? -1 : ((date1 < date2) ? 1 : 0));
                });
            }
        }
        else if(type == "updateGetPhoto"){
            if(order == 'ASC') {
                this.jsonObjs.sort(function (obj1, obj2) {
                    var date1 = obj1.updateGetPhoto;
                    var date2 = obj2.updateGetPhoto;
                    if (date1==null) date1="";
                    if (date2==null) date2="";
                    //console.log("obj1: "+date1+" obj2: "+date2);
                    return ((date1 < date2) ? -1 : ((date1 > date2) ? 1 : 0));
                });
            }
            else{
                this.jsonObjs.sort(function (obj1, obj2) {
                    var date1 = obj1.updateGetPhoto;
                    var date2 = obj2.updateGetPhoto;
                    if (date1==null) date1="";
                    if (date2==null) date2="";
                    //console.log("obj1: "+date1+" obj2: "+date2);
                    return ((date1 > date2) ? -1 : ((date1 < date2) ? 1 : 0));
                });
            }
        }

        this.drawList(this.jsonObjs);
    },
    changeDate: function(str1){
        //var hour   = parseInt(str1.substring(6,10));    2014-11-25 12:00:00
        // str1 format should be dd/mm/yyyy. Separator can be anything e.g. / or -. It wont effect
        var year   = parseInt(str1.substr(0,4));
        var month  = parseInt(str1.substr(5,2));
        var day   = parseInt(str1.substr(8,2));
        var hour   = parseInt(str1.substr(11,2));
        var min   = parseInt(str1.substr(14,2));
        var sec  = parseInt(str1.substr(17,2));

        console.log("year = " + year);
        console.log("month = " + month);
        console.log("day = " + day);

        var date1 = new Date(year, month, day,hour,min,sec);
        return date1.getFullYear()+"-"+(date1.getMonth()+1) +"-"+date1.getDate() + " "+date1.getHours()+1<10?"0":""+date1.getHours()+1+":"+ date1.getMinutes()<10?"0":""+ date1.getMinutes();
    },
    sortButtonClick : function(_sortType,sortIcon){
        //alert("Yeah");
        console.log("hide all");

        if(this.sortType == _sortType){
            if(this.sortDirection == 'ASC'){
                this.sortBy(_sortType,'DESC');
                $("#"+sortIcon).removeClass("glyphicon-chevron-up");
                $("#"+sortIcon).addClass("glyphicon-chevron-down");
            }
            else {
                this.sortBy(_sortType, 'ASC');
                $("#"+sortIcon).removeClass("glyphicon-chevron-down");
                $("#"+sortIcon).addClass("glyphicon-chevron-up");
            }
        }
        else{
            this.sortType = _sortType;
            this.sortBy(_sortType, 'ASC');
            $("#"+sortIcon).removeClass("glyphicon-chevron-down");
            $("#"+sortIcon).addClass("glyphicon-chevron-up");
        }

        $("#bspName_sort_icon").hide();
        $("#lastEvent_sort_icon").hide();
        $("#lastEventTime_sort_icon").hide();
        $("#lastUpdateCheckingTime_sort_icon").hide();
        $("#updateGetLangForKey_sort_icon").hide();
        $("#updateGetItemList_sort_icon").hide();
        $("#updateGetPhoto_sort_icon").hide();
        $("#scheduleUpdate_sort_icon").hide();

        $("#"+sortIcon).show();

    },
    drawCommonList: function(objects) {
        var self = this;

        var tableHeaderString = "<tr><th class='tableHead' style='width:10%'><span id='dictLastUpdate'>Dict last update Time</span><span class='glyphicon glyphicon-chevron-up' id ='updateGetLangForKey_sort_icon'></span></th>"+
            "<th class='tableHead' style='width:10%'><span id='itemLastUpdate'>Item last update time</span><span class='glyphicon glyphicon-chevron-up' id ='updateGetItemList_sort_icon'></span></th>"+
            "<th class='tableHead' style='width:10%'><span id='mediaLastUpdate'>Media last update time</span><span class='glyphicon glyphicon-chevron-up' id ='updateGetPhoto_sort_icon'></span></th></tr>";

        $('#commonTable').empty();
        $('#commonTable').append(tableHeaderString);

        for(var y = 0; y<objects.length;y++){
            $("#commonTable").append(
                "<tr style='height:30px'>"+
                "<td style='width:10%'>"+objects[y].dictLastUpdate+"</td>"+
                "<td style='width:10%'>"+objects[y].itemLastUpdate+"</td>"+
                "<td style='width:10%'>"+objects[y].mediaLastUpdate+"</td>"+
                "</tr>");
        }
    },
    drawList: function(objects){

        var self = this;

        var tableHeaderString = "<tr><th class='tableHead'><span id='bspName'>BSP Name</span><span class='glyphicon glyphicon-chevron-up' id ='bspName_sort_icon'></span></th>"+
            "<th class='tableHead' style='width:10%'><span id='lastEvent'>Last Event</span><span class='glyphicon glyphicon-chevron-up' id ='lastEvent_sort_icon'></span></th>"+
            "<th class='tableHead' style='width:10%'><span id='lastEventTime'>Last Event Time</span><span class='glyphicon glyphicon-chevron-up' id ='lastEventTime_sort_icon'></span></th>"+
            "<th class='tableHead' style='width:10%'><span id='lastUpdateCheckingTime'>Last Update Checking Time</span><span class='glyphicon glyphicon-chevron-up' id ='lastUpdateCheckingTime_sort_icon'></span></th>"+
            "<th class='tableHead' style='width:10%'><span id='updateGetLangForKey'>Last GetLangForKey Time</span><span class='glyphicon glyphicon-chevron-up' id ='updateGetLangForKey_sort_icon'></span></th>"+
            "<th class='tableHead' style='width:10%'><span id='updateGetItemList'>Last GetItemList Time</span><span class='glyphicon glyphicon-chevron-up' id ='updateGetItemList_sort_icon'></span></th>"+
            "<th class='tableHead' style='width:10%'><span id='updateGetPhoto'>Last GetPhoto Time</span><span class='glyphicon glyphicon-chevron-up' id ='updateGetPhoto_sort_icon'></span></th>"+
            "<th class='tableHead' style='width:10%'><span id='scheduleUpdate'>Schedule Update</span></th></tr>";

        $("#resultTable").empty();

        $("#resultTable").append(tableHeaderString);

        $("#bspName").on('click',function(){

            self.sortButtonClick('bspname',"bspName_sort_icon");
        });

        $("#lastEvent").on('click',function(){

            self.sortButtonClick('lastevent',"lastEvent_sort_icon");
        });
        $("#lastEventTime").on('click',function(){

            self.sortButtonClick('lasteventtime',"lastEventTime_sort_icon");
        });
        $("#lastUpdateCheckingTime").on('click',function(){

            self.sortButtonClick('lastupdatecheckingtime',"lastUpdateCheckingTime_sort_icon");
        });
        $("#updateGetLangForKey").on('click',function(){

            self.sortButtonClick('updateGetLangForKey',"updateGetLangForKey_sort_icon");
        });
        $("#updateGetItemList").on('click',function(){

            self.sortButtonClick('updateGetItemList',"updateGetItemList_sort_icon");
        });
        $("#updateGetPhoto").on('click',function(){

            self.sortButtonClick('updateGetPhoto',"updateGetPhoto_sort_icon");
        });
        var csec = new Date().getTime();
        var dictLastSec = Date.parse(self.dictLastUpdateTime);
        var itemLastSec = Date.parse(self.itemLastUpdateTime);
        var mediaLastSec = Date.parse(self.mediaLastUpdateTime);

        for(var y = 0; y<objects.length;y++){
            var msec = Date.parse(objects[y].lastUpdate);
            // james debug
            var getLangSec = Date.parse(objects[y].updateGetLangForKey);
            var itemSec = Date.parse(objects[y].updateGetItemList);
            var photoSec = Date.parse(objects[y].updateGetPhoto);

            var d = new Date(msec);
            //var current = new Date();
            //console.log("time: "+current+" : "+d);
            //var csec = current.getTime();
            //console.log("time: "+ (csec-msec)+" cmec: "+csec+" msec:"+msec);
            var schText = "OK!";
            var textColor = "";

            if ((csec-msec)>=this.updateTimeOut || 
                objects[y].lastUpdate==null ||
                getLangSec < dictLastSec ||
                objects[y].updateGetLangForKey==null ||
                itemSec < itemLastSec ||
                objects[y].updateGetItemList==null ||
                photoSec < mediaLastSec ||
                objects[y].updateGetPhoto==null) 
            {
                schText = "Missed!";
                textColor = "color:red;";
            }

            $("#resultTable").append(
                "<tr style='height:30px'><td style='width:10%'>"+
                "<div class='titleText'>"+objects[y].roomName +"</div>"+"</td>"+
                "<td style='width:10%'>"+objects[y].event+"</td>"+
                "<td style='width:10%'>"+objects[y].lastUpdate+"</td>"+
                "<td style='width:10%'>"+objects[y].updateCheck+"</td>"+
                "<td style='width:10%'>"+objects[y].updateGetLangForKey+"</td>"+
                "<td style='width:10%'>"+objects[y].updateGetItemList+"</td>"+
                "<td style='width:10%'>"+objects[y].updateGetPhoto+"</td>"+
                "<td style='width:10%; "+textColor+"'>"+schText+"</td>"+
                "</tr>");

        }
    },
    backToIndex: function(){
        window.parent.PeriEvent.backToIndex();
    },
    moveToTheme: function(){
        //this.destroy();
        $(this.el).hide();
        $("#blackTemp").show();
        $("#content").show();
    },
    showUp: function(){
        $(this.el).show();
        this.isHide = false;
        $("#blackTemp").hide();
    },
    close :function(){
        console.log("close fire");
    },
    destroy: function() {

        //COMPLETELY UNBIND THE VIEW


        //this.$el.removeData().unbind();

        //Remove view from DOM
        $("#itemListContainer").remove();
        $("#item_list_boxer").remove();
        this.undelegateEvents();
        //this.initialize();
        //Backbone.View.prototype.remove.call(this);

    },
    isHide : false
});