<?php
/**
 * Created by PhpStorm.
 * User: marcopo
 * Date: 5/31/2015
 * Time: 2:14 PM
 */

//if there is no session exist, return session timeout to the caller of the api
if(!isset($_SESSION['email'])){
    echo returnStatus(Session_timeout , 'Session_timeout!');
    exit;
}