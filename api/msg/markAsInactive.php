<?php 

ini_set( "display_errors", true );

require( "../../config.php" );
require( "../../php/func_nx.php");
require("../../php/inc.appvars.php");

session_start();
include("../checkSession.php");

$msgId = isset($_POST['msgId'])?$_POST['msgId']:null;

//setup DB
$conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
$conn->exec("set names utf8");

$sql = "UPDATE message SET status=:status, lastUpdate=now(),lastUpdateBy=:lastUpdateBy where id = :msgId;
        DELETE FROM roomMessageMap where messageId = :msgId;";

$st = $conn->prepare ($sql);

$st->bindValue( ":msgId", $msgId, PDO::PARAM_STR );
$st->bindValue( ":status", 'D', PDO::PARAM_STR );
$st->bindValue( ":lastUpdateBy", $_SESSION['email'], PDO::PARAM_STR );


$st->execute();


if($st->fetchColumn() > 0 || $st->rowCount() > 0){
    echo returnStatus(1 , 'mark as inActive  good');
}
else{
    echo returnStatus(1 , 'mark as inActive fail');
}


$conn = null;


?>
