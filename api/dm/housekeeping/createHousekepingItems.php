<?php 

require("../../../config.php");
require("../../../php/inc.appvars.php");
require("../../../php/func_nx.php");

session_start();
  //include("checkSession.php");

$title_en = isset($_REQUEST['title_en'])?$_REQUEST['title_en']:"";
$title_zh_hk = isset($_REQUEST['title_zh_hk'])?$_REQUEST['title_zh_hk']:"";
$title_zh_cn = isset($_REQUEST['title_zh_cn'])?$_REQUEST['title_zh_cn']:"";
$title_jp = isset($_REQUEST['title_jp'])?$_REQUEST['title_jp']:"";
$title_fr = isset($_REQUEST['title_fr'])?$_REQUEST['title_fr']:"";
$title_ar = isset($_REQUEST['title_ar'])?$_REQUEST['title_ar']:"";
$title_es = isset($_REQUEST['title_es'])?$_REQUEST['title_es']:"";
$title_de = isset($_REQUEST['title_de'])?$_REQUEST['title_de']:"";
$title_ko = isset($_REQUEST['title_ko'])?$_REQUEST['title_ko']:"";
$title_ru = isset($_REQUEST['title_ru'])?$_REQUEST['title_ru']:"";
$title_pt = isset($_REQUEST['title_pt'])?$_REQUEST['title_pt']:"";
$title_tr = isset($_REQUEST['title_tr'])?$_REQUEST['title_tr']:"";
$title_my = isset($_REQUEST['title_my'])?$_REQUEST['title_my']:"";

$parentId = 0;

if(isset($_POST['parentId'])){
    $parentId = $_POST['parentId'];
}

$type = isset($_REQUEST['type'])?$_REQUEST['type']:null;
$command = isset($_REQUEST['command'])?$_REQUEST['command']:"";

if(empty($title_en) || empty($type)){
    echo returnStatus(Invalid_input , "All field cannot be empty.");
    exit;
}

// Insert the Article
$conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
$conn->exec("set names utf8");

$sql = "SELECT UUID() AS UUID";
$st = $conn->prepare ( $sql );
$st->execute();

$list = array();

while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
    $list[] = $row;
}

$uuid = $list[0]["UUID"];

$sql = "INSERT INTO dictionary (id,en, zh_hk, zh_cn, jp, fr, ar, es, de, ko, ru, pt, tr, lastUpdate, lastUpdateBy) VALUES (:id, :title, :zh_hk, :zh_cn, :jp, :fr, :ar, :es, :de, :ko, :ru, :pt, :tr, now(),:email)";
$st = $conn->prepare ( $sql );
$st->bindValue( ":id", $uuid, PDO::PARAM_STR );
$st->bindValue( ":title", $title_en, PDO::PARAM_STR );
$st->bindValue( ":zh_hk", $title_zh_hk, PDO::PARAM_STR );
$st->bindValue( ":zh_cn", $title_zh_cn, PDO::PARAM_STR );
$st->bindValue( ":jp", $title_jp, PDO::PARAM_STR );
$st->bindValue( ":fr", $title_fr, PDO::PARAM_STR );
$st->bindValue( ":ar", $title_ar, PDO::PARAM_STR );
$st->bindValue( ":es", $title_es, PDO::PARAM_STR );
$st->bindValue( ":de", $title_de, PDO::PARAM_STR );
$st->bindValue( ":ko", $title_ko, PDO::PARAM_STR );
$st->bindValue( ":ru", $title_ru, PDO::PARAM_STR );
$st->bindValue( ":pt", $title_pt, PDO::PARAM_STR );
$st->bindValue( ":tr", $title_tr, PDO::PARAM_STR );
$st->bindValue( ":email", "system", PDO::PARAM_STR );
$st->execute();
$titleId = $uuid;

$desId = 'descriptionIdTemp';

$sql = "select Max(items.order) as maxOrder from items where parentId = :parentId;";
$st = $conn->prepare ( $sql );
$st->bindValue( ":parentId", $parentId, PDO::PARAM_STR );
$st->execute();

$list = array();

while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
    $list[] = $row;
}

$order = null;
$preSetOrder = isset($_REQUEST['order'])?$_REQUEST['order']:null;
if ($preSetOrder == null) {
	$order = $list[0]["maxOrder"];
	if($order == null){
		$order = 0;
	}
	else{
		$order++;
	}
} else {
	$order = $preSetOrder;
}

$sql = "SELECT UUID() AS UUID";
$st = $conn->prepare ( $sql );
$st->execute();

$list = array();

while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
    $list[] = $row;
}

$uuid_obj = $list[0]["UUID"];


$sql = "INSERT INTO items (items.id,titleId, descriptionId, type, parentId, command, lastUpdate ,lastUpdateBy,items.order) VALUES (:uuid,:titleId,:desId, :type, :parentId, :command, CURRENT_TIMESTAMP,:email,:order)";
$st = $conn->prepare ( $sql );

$st->bindValue( ":uuid", $uuid_obj, PDO::PARAM_STR );
$st->bindValue( ":titleId", $titleId, PDO::PARAM_STR );
$st->bindValue( ":desId", $desId, PDO::PARAM_STR );
$st->bindValue( ":type", $type, PDO::PARAM_STR );
$st->bindValue( ":parentId", $parentId, PDO::PARAM_STR );
$st->bindValue( ":command", $command, PDO::PARAM_STR );
$st->bindValue( ":email", "system", PDO::PARAM_STR );
$st->bindValue( ":order",$order, PDO::PARAM_INT);
$st->execute();
$conn = null;


if($st->fetchColumn() > 0 || $st->rowCount() > 0) {
    echo returnStatus(1, $uuid_obj);
}
else{
    echo returnStatus(0, 'create item fail');
}


?>