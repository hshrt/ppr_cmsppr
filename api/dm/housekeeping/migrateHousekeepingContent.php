<?php

include("../../../php/func_nx.php");
ini_set( "display_errors", true );

//very important, to define which DB you want to migrate to
define( "THE_ROOT_PATH", "http://192.168.15.114/cmsppr/");

//this is DB login of the local postgres DB
$GLOBALS["link"] = pg_connect("dbname=postgres user=postgres password=esdesd");

$result = pg_exec($GLOBALS["link"], "SET client_encoding = 'UTF8';");
pg_set_client_encoding($GLOBALS["link"], "UTF8");

$content_type="guest_request";
$result = pg_exec($GLOBALS["link"], "select * from devics.content where content_type='{$content_type}';");
$numrows = pg_numrows($result);

if (sizeof($numrows) <= 0) {
    echo "Cannot find record";
} else {
	$row_counter=0;
	while ($row_counter < $numrows && $row = pg_fetch_array($result, $row_counter,PGSQL_ASSOC)) {
		$rootObj = json_decode($row["data"], true);
		$items = $rootObj['items'];
		//$childOfRootObj = json_decode($items["command"], true);
		//echo htmlentities($items[1]['title']);
		$result1 = pg_exec($GLOBALS["link"], "select * from devics.locale where key='{$rootObj['title']}';");
		$numrows1 = pg_numrows($result1);
		if (sizeof($numrows1) <= 0) {
			echo "Cannot find record";
		} else {
			$row_counter1=0;
			$titleEn="";
			$titleFrench="";
			$titleJapan="";
			$titleArabic="";
			$titleSpanish="";
			$titleDeutsch="";
			$titleRussia="";
			$titlePortuguese="";
			$titleSChinese="";
			$titleTChinese="";
			$titleKorean="";			
			while ($row_counter1 < $numrows1 && $row1 = pg_fetch_array($result1, $row_counter1,PGSQL_ASSOC)) {
				$currLang = $row1["lang"];
				if ($currLang == "en-us") {
					$titleEn = $row1["data"];	
				} else if ($currLang == "fr-fr") {
					$titleFrench = $row1["data"];
				} else if ($currLang == "ja") {
					$titleJapan = $row1["data"];
				} else if ($currLang == "ar") {
					$titleArabic = $row1["data"];
				} else if ($currLang == "es") {
					$titleSpanish = $row1["data"];
				} else if ($currLang == "de") {
					$titleDeutsch = $row1["data"];
				} else if ($currLang == "ra") {
					$titleRussia = $row1["data"];
				} else if ($currLang == "pt") {
					$titlePortuguese = $row1["data"];
				} else if ($currLang == "zh-cs") {
					$titleSChinese = $row1["data"];
				} else if ($currLang == "zh-ct") {
					$titleTChinese = $row1["data"];
				} else if ($currLang == "ko") {
					$titleKorean = $row1["data"];
				}				
				$row_counter1++;
			}
            addHousekeepingFirst($items, $titleEn, $titleFrench, $titleJapan, $titleArabic, $titleSpanish, $titleDeutsch, $titleRussia, $titlePortuguese, $titleSChinese, $titleTChinese, $titleKorean);
		}
		$row_counter++;
	}
}
function addHousekeepingFirst($items, $titleEn, $titleFrench, $titleJapan, $titleArabic, $titleSpanish, $titleDeutsch, $titleRussia, $titlePortuguese, $titleSChinese, $titleTChinese, $titleKorean) {
	$section="Guest request";
    $data = array ('title_en' => $titleEn, 'title_fr' => $titleFrench, 'title_jp' => $titleJapan, 'title_ar' => $titleArabic, 'title_es' => $titleSpanish, 'title_de' => $titleDeutsch, 'title_ru' => $titleRussia, 'title_pt' => $titlePortuguese, 'title_zh_cn' => $titleSChinese, 'title_zh_hk' => $titleTChinese, 'title_ko' => $titleKorean, 'type' => $section, 'parentId'=>'0', 'order'=>'0');

    $data = http_build_query($data);

    $context_options = array (
        'http' => array (
            'method' => 'POST',
            'header'=> "Content-type: application/x-www-form-urlencoded\r\n"
                . "Content-Length: " . strlen($data) . "\r\n",
            'content' => $data
        )
    );

    $context = stream_context_create($context_options);
    //$result123 = file_get_contents(THE_ROOT_PATH.'api/dm/createNewItemDM.php', false, $context);
	$fp = fopen(THE_ROOT_PATH.'api/dm/housekeeping/createHousekepingItems.php', 'r', false, $context);
    //If $result is FALSE, then the request has failed.
    if($fp === false){
        //If the request failed, throw an Exception containing
        //the error.
		echo "POST Failed!!!";
        $error = error_get_last();
        throw new Exception('POST request failed: ' . $error['message']);
    } else {
		$response = stream_get_contents($fp);
		$responseObj = json_decode($response,true);
		addHousekeepingSecond($items, $responseObj["msg"], $titleEn, $titleFrench, $titleJapan, $titleArabic, $titleSpanish, $titleDeutsch, $titleRussia, $titlePortuguese, $titleSChinese, $titleTChinese, $titleKorean);
	}		
}

function addHousekeepingSecond($items, $parentId, $titleEn, $titleFrench, $titleJapan, $titleArabic, $titleSpanish, $titleDeutsch, $titleRussia, $titlePortuguese, $titleSChinese, $titleTChinese, $titleKorean) {
	$section="section";
    $data = array ('title_en' => $titleEn, 'title_fr' => $titleFrench, 'title_jp' => $titleJapan, 'title_ar' => $titleArabic, 'title_es' => $titleSpanish, 'title_de' => $titleDeutsch, 'title_ru' => $titleRussia, 'title_pt' => $titlePortuguese, 'title_zh_cn' => $titleSChinese, 'title_zh_hk' => $titleTChinese, 'title_ko' => $titleKorean, 'type' => $section, 'parentId'=>$parentId, 'order'=>'0');

    $data = http_build_query($data);

    $context_options = array (
        'http' => array (
            'method' => 'POST',
            'header'=> "Content-type: application/x-www-form-urlencoded\r\n"
                . "Content-Length: " . strlen($data) . "\r\n",
            'content' => $data
        )
    );

    $context = stream_context_create($context_options);
    //$result123 = file_get_contents(THE_ROOT_PATH.'api/dm/createNewItemDM.php', false, $context);
	$fp = fopen(THE_ROOT_PATH.'api/dm/housekeeping/createHousekepingItems.php', 'r', false, $context);
    //If $result is FALSE, then the request has failed.
    if($fp === false){
        //If the request failed, throw an Exception containing
        //the error.
		echo "POST Failed!!!";
        $error = error_get_last();
        throw new Exception('POST request failed: ' . $error['message']);
    } else {
		$response = stream_get_contents($fp);
		$responseObj = json_decode($response,true);
		$itemCount = sizeof($items);
		$count=0;
		while ($count<$itemCount) {
			$result2 = pg_exec($GLOBALS["link"], "select * from devics.locale where key='{$items[$count]['title']}';");
			$numrows2 = pg_numrows($result2);
			if (sizeof($numrows2) <= 0) {
				echo "Cannot find record";
			} else {
				$row_counter2=0;
				$titleEn="";
				$titleFrench="";
				$titleJapan="";
				$titleArabic="";
				$titleSpanish="";
				$titleDeutsch="";
				$titleRussia="";
				$titlePortuguese="";
				$titleSChinese="";
				$titleTChinese="";
				$titleKorean="";			
				while ($row_counter2 < $numrows2 && $row2 = pg_fetch_array($result2, $row_counter2,PGSQL_ASSOC)) {
					$currLang = $row2["lang"];
					if ($currLang == "en-us") {
						$titleEn = $row2["data"];	
					} else if ($currLang == "fr-fr") {
						$titleFrench = $row2["data"];
					} else if ($currLang == "ja") {
						$titleJapan = $row2["data"];
					} else if ($currLang == "ar") {
						$titleArabic = $row2["data"];
					} else if ($currLang == "es") {
						$titleSpanish = $row2["data"];
					} else if ($currLang == "de") {
						$titleDeutsch = $row2["data"];
					} else if ($currLang == "ra") {
						$titleRussia = $row2["data"];
					} else if ($currLang == "pt") {
						$titlePortuguese = $row2["data"];
					} else if ($currLang == "zh-cs") {
						$titleSChinese = $row2["data"];
					} else if ($currLang == "zh-ct") {
						$titleTChinese = $row2["data"];
					} else if ($currLang == "ko") {
						$titleKorean = $row2["data"];
					}				
					$row_counter2++;
				}			
				addChildOfHousekeeping($responseObj["msg"], $items[$count]['command'], $titleEn, $titleFrench, $titleJapan, $titleArabic, $titleSpanish, $titleDeutsch, $titleRussia, $titlePortuguese, $titleSChinese, $titleTChinese, $titleKorean);
			}
			$count++;
		}
	}
}

function addChildOfHousekeeping($parentId, $command, $titleEn, $titleFrench, $titleJapan, $titleArabic, $titleSpanish, $titleDeutsch, $titleRussia, $titlePortuguese, $titleSChinese, $titleTChinese, $titleKorean) {
	$section="service";
    $data = array ('command' => $command, 'title_en' => $titleEn, 'title_fr' => $titleFrench, 'title_jp' => $titleJapan, 'title_ar' => $titleArabic, 'title_es' => $titleSpanish, 'title_de' => $titleDeutsch, 'title_ru' => $titleRussia, 'title_pt' => $titlePortuguese, 'title_zh_cn' => $titleSChinese, 'title_zh_hk' => $titleTChinese, 'title_ko' => $titleKorean, 'type' => $section, 'parentId'=>$parentId);

    $data = http_build_query($data);

    $context_options = array (
        'http' => array (
            'method' => 'POST',
            'header'=> "Content-type: application/x-www-form-urlencoded\r\n"
                . "Content-Length: " . strlen($data) . "\r\n",
            'content' => $data
        )
    );

    $context = stream_context_create($context_options);
    //$result123 = file_get_contents(THE_ROOT_PATH.'api/dm/createNewItemDM.php', false, $context);
	$fp = fopen(THE_ROOT_PATH.'api/dm/housekeeping/createHousekepingItems.php', 'r', false, $context);
    //If $result is FALSE, then the request has failed.
    if($fp === false){
        //If the request failed, throw an Exception containing
        //the error.
		echo "POST Failed!!!";
        $error = error_get_last();
        throw new Exception('POST request failed: ' . $error['message']);
    } else {
		$response = stream_get_contents($fp);
		$responseObj = json_decode($response,true);
	}	
	
}

?>
