<?php

include("../../../php/func_nx.php");
ini_set( "display_errors", true );

//very important, to define which DB you want to migrate to
define( "THE_ROOT_PATH", "http://192.168.15.114/cmsppr/");

//this is DB login of the local postgres DB
$GLOBALS["link"] = pg_connect("dbname=postgres user=postgres password=esdesd");

$result = pg_exec($GLOBALS["link"], "SET client_encoding = 'UTF8';");
pg_set_client_encoding($GLOBALS["link"], "UTF8");

$result = pg_exec($GLOBALS["link"], "select * from devics.content;");
$numrows = pg_numrows($result);

if (sizeof($numrows) <= 0) {
    echo "Cannot find record";
} else {
	$row_counter=0;
    while ($row_counter < $numrows && $row = pg_fetch_array($result, $row_counter,PGSQL_ASSOC)) {
		$rootObj = json_decode($row["data"], true);
		$schema = $rootObj['_schema'];
		//$titles = htmlspecialchars($rootObj['title']);
		//$descs = htmlspecialchars($rootObj['description']);
		$selfId=$row['id'];
		if($schema != null && $schema == "DiningPage") {
			//echo $selfId."=selfId<br>";
			//echo $schema."=schema<br>";
			//echo $titles."=title<br>";
			//echo $descs."=description<br>";
			$lang="en-us";
            $result1 = pg_exec($GLOBALS["link"], "select * from devics.locale where key='{$rootObj['title']}';");
            $numrows1 = pg_numrows($result1);
			if ($numrows1 <= 0) {
				echo "Cannot find record";
			} else {
				$row_counter1=0;
				$titleEn="";
				$titleFrench="";
				$titleJapan="";
				$titleArabic="";
				$titleSpanish="";
				$titleDeutsch="";
				$titleRussia="";
				$titlePortuguese="";
				$titleSChinese="";
				$titleTChinese="";
				$titleKorean="";
				while ($row_counter1 < $numrows1 && $row1 = pg_fetch_array($result1, $row_counter1,PGSQL_ASSOC)) {
					$currLang = $row1["lang"];
					if ($currLang == "en-us") {
						$titleEn = $row1["data"];	
					} else if ($currLang == "fr-fr") {
						$titleFrench = $row1["data"];
					} else if ($currLang == "ja") {
						$titleJapan = $row1["data"];
					} else if ($currLang == "ar") {
						$titleArabic = $row1["data"];
					} else if ($currLang == "es") {
						$titleSpanish = $row1["data"];
					} else if ($currLang == "de") {
						$titleDeutsch = $row1["data"];
					} else if ($currLang == "ra") {
						$titleRussia = $row1["data"];
					} else if ($currLang == "pt") {
						$titlePortuguese = $row1["data"];
					} else if ($currLang == "zh-cs") {
						$titleSChinese = $row1["data"];
					} else if ($currLang == "zh-ct") {
						$titleTChinese = $row1["data"];
					} else if ($currLang == "ko") {
						$titleKorean = $row1["data"];
					}
					//echo $title123."=titleTitle<br>";
					$row_counter1++;
				}
				addIRD($titleEn, $titleFrench, $titleJapan, $titleArabic, $titleSpanish, $titleDeutsch, $titleRussia, $titlePortuguese, $titleSChinese, $titleTChinese, $titleKorean, $selfId, $rootObj['image']);
			}
			break;
		}
		$row_counter++;
	}

    //foreach ($GLOBALS["descriptions"] as $menuId) {
        //$menu = $GLOBALS["descriptions"][$menuId];
	//	echo $menuId;
	//}
}

function addIRD($titleEn, $titleFrench, $titleJapan, $titleArabic, $titleSpanish, $titleDeutsch, $titleRussia, $titlePortuguese, $titleSChinese, $titleTChinese, $titleKorean, $selfId, $imagePath) {
    //setup input parameter

    //echo ('addIRDTopBarItem fire first!');
	$section="Information (with top sub menu)";
    $data = array ('title_en' => $titleEn, 'title_fr' => $titleFrench, 'title_jp' => $titleJapan, 'title_ar' => $titleArabic, 'title_es' => $titleSpanish, 'title_de' => $titleDeutsch, 'title_ru' => $titleRussia, 'title_pt' => $titlePortuguese, 'title_zh_cn' => $titleSChinese, 'title_zh_hk' => $titleTChinese, 'title_ko' => $titleKorean, 'type' => $section, 'parentId'=>'1', 'order'=>'0', 'skipCreateDescription'=>'1');

    $data = http_build_query($data);

    $context_options = array (
        'http' => array (
            'method' => 'POST',
            'header'=> "Content-type: application/x-www-form-urlencoded\r\n"
                . "Content-Length: " . strlen($data) . "\r\n",
            'content' => $data
        )
    );

    $context = stream_context_create($context_options);
    //$result123 = file_get_contents(THE_ROOT_PATH.'api/dm/createNewItemDM.php', false, $context);
	$fp = fopen(THE_ROOT_PATH.'api/dm/dining/createNewIRDItem.php', 'r', false, $context);
    //If $result is FALSE, then the request has failed.
    if($fp === false){
        //If the request failed, throw an Exception containing
        //the error.
		echo "POST Failed!!!";
        $error = error_get_last();
        throw new Exception('POST request failed: ' . $error['message']);
    } else {
		$response = stream_get_contents($fp);
		$responseObj = json_decode($response,true);
		if (!empty($imagePath)){
		    $typeImage="icon";
			downloadImage($typeImage, $responseObj["msg"], $imagePath);
		}		
		//pprint_r($responseObj["msg"]);
		//while(--$count>=0) {
        $result1 = pg_exec($GLOBALS["link"], "select * from devics.content where parent_id='{$selfId}';");
		$numrows1 = pg_numrows($result1);
		if ($numrows1 <= 0) {
				echo "Cannot find record";			
		} else {
			$row_counter11=0;
			while ($row_counter11 < $numrows1 && $row = pg_fetch_array($result1, $row_counter11,PGSQL_ASSOC)) {
				$selfId1=$row['id'];
				$rootObj = json_decode($row["data"], true);	
				$position11=$row['position'];
				$lang="en-us";
				$result2 = pg_exec($GLOBALS["link"], "select * from devics.locale where key='{$rootObj['title']}';");
				$numrows2 = pg_numrows($result2);
				if ($numrows2 <= 0) {
					echo "Cannot find record";
				} else {
					$row_counter22=0;
					$titleEn="";
					$titleFrench="";
					$titleJapan="";
					$titleArabic="";
					$titleSpanish="";
					$titleDeutsch="";
					$titleRussia="";
					$titlePortuguese="";
					$titleSChinese="";
					$titleTChinese="";
					$titleKorean="";					
					while ($row_counter22 < $numrows2 && $row3 = pg_fetch_array($result2, $row_counter22,PGSQL_ASSOC)) {
						$currLang = $row3["lang"];
						if ($currLang == "en-us") {
							$titleEn = $row3["data"];	
						} else if ($currLang == "fr-fr") {
							$titleFrench = $row3["data"];
						} else if ($currLang == "ja") {
							$titleJapan = $row3["data"];
						} else if ($currLang == "ar") {
							$titleArabic = $row3["data"];
						} else if ($currLang == "es") {
							$titleSpanish = $row3["data"];
						} else if ($currLang == "de") {
							$titleDeutsch = $row3["data"];
						} else if ($currLang == "ra") {
							$titleRussia = $row3["data"];
						} else if ($currLang == "pt") {
							$titlePortuguese = $row3["data"];
						} else if ($currLang == "zh-cs") {
							$titleSChinese = $row3["data"];
						} else if ($currLang == "zh-ct") {
							$titleTChinese = $row3["data"];
						} else if ($currLang == "ko") {
							$titleKorean = $row3["data"];
						}
						$row_counter22++;
					}
					addIRDTopBarItem($titleEn, $titleFrench, $titleJapan, $titleArabic, $titleSpanish, $titleDeutsch, $titleRussia, $titlePortuguese, $titleSChinese, $titleTChinese, $titleKorean, $position11, $responseObj["msg"], $selfId1);
				}
				$row_counter11++;
			}
		}
	}
}

function addIRDTopBarItem($titleEn, $titleFrench, $titleJapan, $titleArabic, $titleSpanish, $titleDeutsch, $titleRussia, $titlePortuguese, $titleSChinese, $titleTChinese, $titleKorean, $position, $parent_id, $selfId1){
    //setup input parameter

    //echo ('addIRDTopBarItem fire first!');
	$section="section";
    $data = array ('title_en' => $titleEn, 'title_fr' => $titleFrench, 'title_jp' => $titleJapan, 'title_ar' => $titleArabic, 'title_es' => $titleSpanish, 'title_de' => $titleDeutsch, 'title_ru' => $titleRussia, 'title_pt' => $titlePortuguese, 'title_zh_cn' => $titleSChinese, 'title_zh_hk' => $titleTChinese, 'title_ko' => $titleKorean, 'type' => $section, 'parentId'=>$parent_id, 'order'=>$position, 'skipCreateDescription'=>'1');

    $data = http_build_query($data);

    $context_options = array (
        'http' => array (
            'method' => 'POST',
            'header'=> "Content-type: application/x-www-form-urlencoded\r\n"
                . "Content-Length: " . strlen($data) . "\r\n",
            'content' => $data
        )
    );

    $context = stream_context_create($context_options);
    //$result123 = file_get_contents(THE_ROOT_PATH.'api/dm/createNewItemDM.php', false, $context);
	$fp = fopen(THE_ROOT_PATH.'api/dm/dining/createNewIRDItem.php', 'r', false, $context);
    //If $result is FALSE, then the request has failed.
    if($fp === false){
        //If the request failed, throw an Exception containing
        //the error.
		echo "POST Failed!!!";
        $error = error_get_last();
        throw new Exception('POST request failed: ' . $error['message']);
    } else {
		$response = stream_get_contents($fp);
		$responseObj = json_decode($response,true);
		//pprint_r($responseObj["msg"]);
		//while(--$count>=0) {
        $result1 = pg_exec($GLOBALS["link"], "select * from devics.content where parent_id='{$selfId1}';");
		$numrows1 = pg_numrows($result1);
		if ($numrows1 <= 0) {
				echo "Cannot find record";			
		} else {
			$row_counter=0;
			while ($row_counter < $numrows1 && $row = pg_fetch_array($result1, $row_counter,PGSQL_ASSOC)) {
				$selfId2=$row['id'];
				$rootObj = json_decode($row["data"], true);	
				$position=$row['position'];
				$lang="en-us";
				$result2 = pg_exec($GLOBALS["link"], "select * from devics.locale where key='{$rootObj['title']}';");
				$numrows2 = pg_numrows($result2);
				if ($numrows2 <= 0) {
					echo "Cannot find record";
				} else {
					$row_counter2=0;
					$titleEn="";
					$titleFrench="";
					$titleJapan="";
					$titleArabic="";
					$titleSpanish="";
					$titleDeutsch="";
					$titleRussia="";
					$titlePortuguese="";
					$titleSChinese="";
					$titleTChinese="";
					$titleKorean="";					
					while ($row_counter2 < $numrows2 && $row3 = pg_fetch_array($result2, $row_counter2,PGSQL_ASSOC)) {
						$currLang = $row3["lang"];
						if ($currLang == "en-us") {
							$titleEn = $row3["data"];	
						} else if ($currLang == "fr-fr") {
							$titleFrench = $row3["data"];
						} else if ($currLang == "ja") {
							$titleJapan = $row3["data"];
						} else if ($currLang == "ar") {
							$titleArabic = $row3["data"];
						} else if ($currLang == "es") {
							$titleSpanish = $row3["data"];
						} else if ($currLang == "de") {
							$titleDeutsch = $row3["data"];
						} else if ($currLang == "ra") {
							$titleRussia = $row3["data"];
						} else if ($currLang == "pt") {
							$titlePortuguese = $row3["data"];
						} else if ($currLang == "zh-cs") {
							$titleSChinese = $row3["data"];
						} else if ($currLang == "zh-ct") {
							$titleTChinese = $row3["data"];
						} else if ($currLang == "ko") {
							$titleKorean = $row3["data"];
						}
						$row_counter2++;
					}
					addIRDSideBarItem($titleEn, $titleFrench, $titleJapan, $titleArabic, $titleSpanish, $titleDeutsch, $titleRussia, $titlePortuguese, $titleSChinese, $titleTChinese, $titleKorean, $position, $responseObj["msg"], $selfId2, $rootObj['image']);
				}
				$row_counter++;
			}
		}
	}
		//}
}

function addIRDSideBarItem($titleEn, $titleFrench, $titleJapan, $titleArabic, $titleSpanish, $titleDeutsch, $titleRussia, $titlePortuguese, $titleSChinese, $titleTChinese, $titleKorean, $position, $parentId, $selfId2, $imagePath){
    //setup input parameter

    //echo ('addIRDTopBarItem fire first!');
	$section="article";
    $data = array ('title_en' => $titleEn, 'title_fr' => $titleFrench, 'title_jp' => $titleJapan, 'title_ar' => $titleArabic, 'title_es' => $titleSpanish, 'title_de' => $titleDeutsch, 'title_ru' => $titleRussia, 'title_pt' => $titlePortuguese, 'title_zh_cn' => $titleSChinese, 'title_zh_hk' => $titleTChinese, 'title_ko' => $titleKorean, 'type' => $section, 'parentId'=>$parentId, 'order'=>$position, 'skipCreateDescription'=>'1');

    $data = http_build_query($data);

    $context_options = array (
        'http' => array (
            'method' => 'POST',
            'header'=> "Content-type: application/x-www-form-urlencoded\r\n"
                . "Content-Length: " . strlen($data) . "\r\n",
            'content' => $data
        )
    );

    $context = stream_context_create($context_options);
    $fp = fopen(THE_ROOT_PATH.'api/dm/dining/createNewIRDItem.php', 'r', false, $context);
    //If $result is FALSE, then the request has failed.
    if($fp === false){
        //If the request failed, throw an Exception containing
        //the error.
		echo "POST Failed!!!";
        $error = error_get_last();
        throw new Exception('POST request failed: ' . $error['message']);
    } else {
		$response = stream_get_contents($fp);
		$responseObj = json_decode($response,true);
		if (!empty($imagePath)){
			downloadImage("prefer", $responseObj["msg"], $imagePath);
		}
		//pprint_r($responseObj["msg"]);
		//while(--$count>=0) {
        $result1 = pg_exec($GLOBALS["link"], "select * from devics.content where parent_id='{$selfId2}';");
		$numrows1 = pg_numrows($result1);
		if ($numrows1 <= 0) {
				echo "Cannot find record";			
		} else {
			$row_counter=0;
			while ($row_counter < $numrows1 && $row = pg_fetch_array($result1, $row_counter,PGSQL_ASSOC)) {
				$rootObj = json_decode($row["data"], true);	
				$position=$row['position'];
				$lang="en-us";
				$titleEn="";
				$titleFrench="";
				$titleJapan="";
				$titleArabic="";
				$titleSpanish="";
				$titleDeutsch="";
				$titleRussia="";
				$titlePortuguese="";
				$titleSChinese="";
				$titleTChinese="";
				$titleKorean="";
				$descEn="";
				$descFrench="";
				$descJapan="";
				$descArabic="";
				$descSpanish="";
				$descDeutsch="";
				$descRussia="";
				$descPortuguese="";
				$descSChinese="";
				$descTChinese="";
				$descKorean="";				
				$result2 = pg_exec($GLOBALS["link"], "select * from devics.locale where key='{$rootObj['title']}';");			
				$numrows2 = pg_numrows($result2);		
				if ($numrows2 <= 0) {
					echo "Cannot find record";
				} else {
					$row_counter2=0;
					while ($row_counter2 < $numrows2 && $row3 = pg_fetch_array($result2, $row_counter2,PGSQL_ASSOC)) {
						$currLang = $row3["lang"];
						if ($currLang == "en-us") {
							$titleEn = $row3["data"];	
						} else if ($currLang == "fr-fr") {
							$titleFrench = $row3["data"];
						} else if ($currLang == "ja") {
							$titleJapan = $row3["data"];
						} else if ($currLang == "ar") {
							$titleArabic = $row3["data"];
						} else if ($currLang == "es") {
							$titleSpanish = $row3["data"];
						} else if ($currLang == "de") {
							$titleDeutsch = $row3["data"];
						} else if ($currLang == "ra") {
							$titleRussia = $row3["data"];
						} else if ($currLang == "pt") {
							$titlePortuguese = $row3["data"];
						} else if ($currLang == "zh-cs") {
							$titleSChinese = $row3["data"];
						} else if ($currLang == "zh-ct") {
							$titleTChinese = $row3["data"];
						} else if ($currLang == "ko") {
							$titleKorean = $row3["data"];
						}
						$row_counter2++;
					}
				}
				$result3 = pg_exec($GLOBALS["link"], "select * from devics.locale where key='{$rootObj['description']}';");
				$numrows3 = pg_numrows($result3);
				if ($numrows3 <= 0) {
					echo "Cannot find record";
				} else {
					$row_counter3=0;
					while ($row_counter3 < $numrows3 && $row4 = pg_fetch_array($result3, $row_counter3,PGSQL_ASSOC)) {
						$currLang = $row4["lang"];
						if ($currLang == "en-us") {
							$descEn = $row4["data"];	
						} else if ($currLang == "fr-fr") {
							$descFrench = $row4["data"];
						} else if ($currLang == "ja") {
							$descJapan = $row4["data"];
						} else if ($currLang == "ar") {
							$descArabic = $row4["data"];
						} else if ($currLang == "es") {
							$descSpanish = $row4["data"];
						} else if ($currLang == "de") {
							$descDeutsch = $row4["data"];
						} else if ($currLang == "ra") {
							$descRussia = $row4["data"];
						} else if ($currLang == "pt") {
							$descPortuguese = $row4["data"];
						} else if ($currLang == "zh-cs") {
							$descSChinese = $row4["data"];
						} else if ($currLang == "zh-ct") {
							$descTChinese = $row4["data"];
						} else if ($currLang == "ko") {
							$descKorean = $row4["data"];
						}
						$row_counter3++;
					}					
				}
				addIRDChildofSideBarItem($titleEn, $titleFrench, $titleJapan, $titleArabic, $titleSpanish, $titleDeutsch, $titleRussia, $titlePortuguese, $titleSChinese, $titleTChinese, $titleKorean,$descEn, $descFrench, $descJapan, $descArabic, $descSpanish, $descDeutsch, $descRussia, $descPortuguese, $descSChinese, $descTChinese, $descKorean, $position, $responseObj["msg"], $rootObj['price']);
				$row_counter++;
			}
		}
	}
    //If everything went OK, return the response.
    //return $result123;

}

function downloadImage($typeImage, $itemId, $imagePath){
	$imageName = substr($imagePath,7,strpos($imagePath,".")-7);
	$fullFileName = substr($imagePath,7,strlen($imagePath)-7);
    $urlbase = 'http://192.168.15.114/cmsppr/uploadtest/';
    $url = $urlbase.$fullFileName;
    //file_put_contents($img, file_get_contents($url));

    $b64image = base64_encode(file_get_contents($url));
	addImagesAndMaps($typeImage, $itemId, $b64image, $imageName, $fullFileName);
}

function addImagesAndMaps($typeImage, $itemId, $imageData, $imageName, $fullFileName) {
    $data = array ('type' => $typeImage, 'image' => $imageData,'itemId' => $itemId, 'fileName' => $imageName, 'fullFileName' => $fullFileName);

    $data = http_build_query($data);

    $context_options = array (
        'http' => array (
            'method' => 'POST',
            'header'=> "Content-type: application/x-www-form-urlencoded\r\n"
                . "Content-Length: " . strlen($data) . "\r\n",
            'content' => $data
        )
    );

    $context = stream_context_create($context_options);
    $fp = fopen(THE_ROOT_PATH.'api/dm/dining/createMediaAndMap.php', 'r', false, $context);
    //If $result is FALSE, then the request has failed.
    if($fp === false){
        //If the request failed, throw an Exception containing
        //the error.
		echo "POST Failed!!!";
        $error = error_get_last();
        throw new Exception('POST request failed: ' . $error['message']);
    } 	
}

function addIRDChildofSideBarItem($titleEn, $titleFrench, $titleJapan, $titleArabic, $titleSpanish, $titleDeutsch, $titleRussia, $titlePortuguese, $titleSChinese, $titleTChinese, $titleKorean,$descEn, $descFrench, $descJapan, $descArabic, $descSpanish, $descDeutsch, $descRussia, $descPortuguese, $descSChinese, $descTChinese, $descKorean, $position, $parentId, $price){
    //setup input parameter

    //echo ('addIRDChildofSideBarItem fire first!');
	$section="item";
    $data = array ('title_en' => $titleEn, 'title_fr' => $titleFrench, 'title_jp' => $titleJapan, 'title_ar' => $titleArabic, 'title_es' => $titleSpanish, 'title_de' => $titleDeutsch, 'title_ru' => $titleRussia, 'title_pt' => $titlePortuguese, 'title_zh_cn' => $titleSChinese, 'title_zh_hk' => $titleTChinese, 'title_ko' => $titleKorean,'desc_en' => $descEn, 'desc_fr' => $descFrench, 'desc_jp' => $descJapan, 'desc_es' => $descSpanish, 'desc_de' => $descDeutsch, 'desc_ru' => $descRussia, 'desc_pt' => $descPortuguese, 'desc_zh_cn' => $descSChinese, 'desc_zh_hk' => $descTChinese, 'desc_ko' => $descKorean, 'type' => $section, 'parentId'=>$parentId, 'order'=>$position, 'price' => $price);

    $data = http_build_query($data);

    $context_options = array (
        'http' => array (
            'method' => 'POST',
            'header'=> "Content-type: application/x-www-form-urlencoded\r\n"
                . "Content-Length: " . strlen($data) . "\r\n",
            'content' => $data
        )
    );

    $context = stream_context_create($context_options);
    $fp = fopen(THE_ROOT_PATH.'api/dm/dining/createNewIRDItem.php', 'r', false, $context);
    //If $result is FALSE, then the request has failed.
    if($fp === false){
        //If the request failed, throw an Exception containing
        //the error.
		echo "POST Failed!!!";
        $error = error_get_last();
        throw new Exception('POST request failed: ' . $error['message']);
    }
}


?>
