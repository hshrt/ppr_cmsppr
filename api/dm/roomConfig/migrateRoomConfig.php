<?php

include("../../../php/func_nx.php");
ini_set( "display_errors", true );

//very important, to define which DB you want to migrate to
define( "THE_ROOT_PATH", "http://192.168.15.114/cmsppr/");

//this is DB login of the local postgres DB
$GLOBALS["link"] = pg_connect("dbname=postgres user=postgres password=esdesd");

$result = pg_exec($GLOBALS["link"], "SET client_encoding = 'UTF8';");
pg_set_client_encoding($GLOBALS["link"], "UTF8");

$result = pg_exec($GLOBALS["link"], "select * from devics.roomconfig;");
$numrows = pg_numrows($result);

if (sizeof($numrows) <= 0) {
    echo "Cannot find record";
} else {
	$row_counter=0;
	while ($row_counter < $numrows && $row = pg_fetch_array($result, $row_counter,PGSQL_ASSOC)) {
		$room = $row['room_number'];
		$rootObj = json_decode($row["zone_data"], true);
		$zones = $rootObj['zones'];
		$zoneNums = sizeof($zones);
		//$mas = json_decode($zones['0']['mas'], true);
		//$rootObj1 = json_decode($rootObj["sip"], true);
		$count=0;
		while ($count < $zoneNums) {
			$title = $zones[$count]["title"];
			$mas = $zones[$count]["mas"];
			$type = $zones[$count]["type"];
			$awningEnabled = "0";
			if ($zones[$count]["awningEnabled"] != null) {
				$awningEnabled = $zones[$count]["awningEnabled"];
			}
			$setTopBox = "0";
			if ($zones[$count]["setTopBox"] != null) {
				$setTopBox = $zones[$count]["setTopBox"];
			}			
			$titleShortened = substr($title, strripos($title, ".")+1, strlen($title)-(strripos($title, ".")+2));
			addRoomConfig($room, $titleShortened, $type, $awningEnabled, $setTopBox, $mas);
			$count++;
		}
		$row_counter++;
	}
}

function addRoomConfig($room, $zoneName, $type, $awningEnabled, $setTopBox, $mas) {
	$section="service";
    $data = array ('room' => $room, 'zoneName' => $zoneName, 'type' => $type, 'awningEnabled' => $awningEnabled, 'setTopBox' => $setTopBox, 'mas' => $mas);

    $data = http_build_query($data);

    $context_options = array (
        'http' => array (
            'method' => 'POST',
            'header'=> "Content-type: application/x-www-form-urlencoded\r\n"
                . "Content-Length: " . strlen($data) . "\r\n",
            'content' => $data
        )
    );

    $context = stream_context_create($context_options);
    //$result123 = file_get_contents(THE_ROOT_PATH.'api/dm/createNewItemDM.php', false, $context);
	$fp = fopen(THE_ROOT_PATH.'api/dm/roomConfig/createRoomConfigItem.php', 'r', false, $context);
    //If $result is FALSE, then the request has failed.
    if($fp === false){
        //If the request failed, throw an Exception containing
        //the error.
		echo "POST Failed!!!";
        $error = error_get_last();
        throw new Exception('POST request failed: ' . $error['message']);
    } else {
		$response = stream_get_contents($fp);
		$responseObj = json_decode($response,true);
	}	
	
}

