<?php 

require("../../../config.php");
require("../../../php/inc.appvars.php");
require("../../../php/func_nx.php");

session_start();
  //include("checkSession.php");

$room = isset($_REQUEST['room'])?$_REQUEST['room']:"";
$zoneName = isset($_REQUEST['zoneName'])?$_REQUEST['zoneName']:"";
$type = isset($_REQUEST['type'])?$_REQUEST['type']:"";
$awningEnabled = isset($_REQUEST['awningEnabled'])?$_REQUEST['awningEnabled']:"";
$setTopBox = isset($_REQUEST['setTopBox'])?$_REQUEST['setTopBox']:"";
$mas = isset($_REQUEST['mas'])?$_REQUEST['mas']:"";

// Insert the Article
$conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
$conn->exec("set names utf8");

$sql = "SELECT UUID() AS UUID";
$st = $conn->prepare ( $sql );
$st->execute();

$list = array();

while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
    $list[] = $row;
}

$uuid = $list[0]["UUID"];

$sql = "INSERT INTO roomConfig (id,room, zoneName, type, awningEnabled, setTopBox, mas, lastUpdate, lastUpdateBy) VALUES (:id, :room, :zoneName, :type, :awningEnabled, :setTopBox, :mas, now(),:email)";
$st = $conn->prepare ( $sql );
$st->bindValue( ":id", $uuid, PDO::PARAM_STR );
$st->bindValue( ":room", $room, PDO::PARAM_STR );
$st->bindValue( ":zoneName", $zoneName, PDO::PARAM_STR );
$st->bindValue( ":type", $type, PDO::PARAM_STR );
$st->bindValue( ":awningEnabled", $awningEnabled, PDO::PARAM_STR );
$st->bindValue( ":setTopBox", $setTopBox, PDO::PARAM_STR );
$st->bindValue( ":mas", $mas, PDO::PARAM_STR );
$st->bindValue( ":email", "system", PDO::PARAM_STR );
$st->execute();
$conn = null;

$list = array();

while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
    $list[] = $row;
}

if($st->fetchColumn() > 0 || $st->rowCount() > 0) {
    echo returnStatus(1, $list);
}
else{
    echo returnStatus(0, 'create item fail');
}
?>
