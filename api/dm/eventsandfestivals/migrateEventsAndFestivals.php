<?php

include("../../../php/func_nx.php");
ini_set( "display_errors", true );

//very important, to define which DB you want to migrate to
define( "THE_ROOT_PATH", "http://192.168.15.114/cmsppr/");

//this is DB login of the local postgres DB
$GLOBALS["link"] = pg_connect("dbname=postgres user=postgres password=esdesd");

$result = pg_exec($GLOBALS["link"], "SET client_encoding = 'UTF8';");
pg_set_client_encoding($GLOBALS["link"], "UTF8");

$id="93d643c6-47e4-4c26-90dd-b0c1d8f3fb2f";
$result = pg_exec($GLOBALS["link"], "select * from devics.content where id='{$id}';");
$numrows = pg_numrows($result);

if (sizeof($numrows) <= 0) {
    echo "Cannot find record";
} else {
	$row_counter=0;
	while ($row_counter < $numrows && $row = pg_fetch_array($result, $row_counter,PGSQL_ASSOC)) {
		$rootObj = json_decode($row["data"], true);
		$selfId=$row['id'];
        $result = pg_exec($GLOBALS["link"], "select * from devics.locale where key='{$rootObj['title']}';");
        $numrows = pg_numrows($result);
		if ($numrows <= 0) {
			echo "Cannot find record";
		} else {
			$row_counter=0;
			$titleEn="";
			$titleFrench="";
			$titleJapan="";
			$titleArabic="";
			$titleSpanish="";
			$titleDeutsch="";
			$titleRussia="";
			$titlePortuguese="";
			$titleSChinese="";
			$titleTChinese="";
			$titleKorean="";
			while ($row_counter < $numrows && $row = pg_fetch_array($result, $row_counter,PGSQL_ASSOC)) {
				$currLang = $row["lang"];
				if ($currLang == "en-us") {
					$titleEn = $row["data"];	
				} else if ($currLang == "fr-fr") {
					$titleFrench = $row["data"];
				} else if ($currLang == "ja") {
					$titleJapan = $row["data"];
				} else if ($currLang == "ar") {
					$titleArabic = $row["data"];
				} else if ($currLang == "es") {
					$titleSpanish = $row["data"];
				} else if ($currLang == "de") {
					$titleDeutsch = $row["data"];
				} else if ($currLang == "ra") {
					$titleRussia = $row["data"];
				} else if ($currLang == "pt") {
					$titlePortuguese = $row["data"];
				} else if ($currLang == "zh-cs") {
					$titleSChinese = $row["data"];
				} else if ($currLang == "zh-ct") {
					$titleTChinese = $row["data"];
				} else if ($currLang == "ko") {
					$titleKorean = $row["data"];
				}
				//echo $title123."=titleTitle<br>";
				$row_counter++;
			}
			addCityAttractions($titleEn, $titleFrench, $titleJapan, $titleArabic, $titleSpanish, $titleDeutsch, $titleRussia, $titlePortuguese, $titleSChinese, $titleTChinese, $titleKorean, $selfId);
		}
		break;
	}
}

function addCityAttractions($titleEn, $titleFrench, $titleJapan, $titleArabic, $titleSpanish, $titleDeutsch, $titleRussia, $titlePortuguese, $titleSChinese, $titleTChinese, $titleKorean, $selfId) {
	$section="Information";
    $data = array ('title_en' => $titleEn, 'title_fr' => $titleFrench, 'title_jp' => $titleJapan, 'title_ar' => $titleArabic, 'title_es' => $titleSpanish, 'title_de' => $titleDeutsch, 'title_ru' => $titleRussia, 'title_pt' => $titlePortuguese, 'title_zh_cn' => $titleSChinese, 'title_zh_hk' => $titleTChinese, 'title_ko' => $titleKorean, 'type' => $section, 'parentId'=>'2', 'order'=>'6', 'skipCreateDescription'=>'1');

    $data = http_build_query($data);

    $context_options = array (
        'http' => array (
            'method' => 'POST',
            'header'=> "Content-type: application/x-www-form-urlencoded\r\n"
                . "Content-Length: " . strlen($data) . "\r\n",
            'content' => $data
        )
    );

    $context = stream_context_create($context_options);
    //$result123 = file_get_contents(THE_ROOT_PATH.'api/dm/createNewItemDM.php', false, $context);
	$fp = fopen(THE_ROOT_PATH.'api/dm/eventsandfestivals/createEventsAndFestivalsItems.php', 'r', false, $context);
    //If $result is FALSE, then the request has failed.
    if($fp === false){
        //If the request failed, throw an Exception containing
        //the error.
		echo "POST Failed!!!";
        $error = error_get_last();
        throw new Exception('POST request failed: ' . $error['message']);
    } else {
		$response = stream_get_contents($fp);
		$responseObj = json_decode($response,true);
        $result1 = pg_exec($GLOBALS["link"], "select * from devics.content where parent_id='{$selfId}';");
		$numrows1 = pg_numrows($result1);
		if ($numrows1 <= 0) {
				echo "Cannot find record";			
		} else {
			$row_counter11=0;
			while ($row_counter11 < $numrows1 && $row = pg_fetch_array($result1, $row_counter11,PGSQL_ASSOC)) {
				$rootObj = json_decode($row["data"], true);	
				$position=$row['position'];
				$titleEn="";
				$titleFrench="";
				$titleJapan="";
				$titleArabic="";
				$titleSpanish="";
				$titleDeutsch="";
				$titleRussia="";
				$titlePortuguese="";
				$titleSChinese="";
				$titleTChinese="";
				$titleKorean="";
				$descEn="";
				$descFrench="";
				$descJapan="";
				$descArabic="";
				$descSpanish="";
				$descDeutsch="";
				$descRussia="";
				$descPortuguese="";
				$descSChinese="";
				$descTChinese="";
				$descKorean="";				
				$result2 = pg_exec($GLOBALS["link"], "select * from devics.locale where key='{$rootObj['title']}';");			
				$numrows2 = pg_numrows($result2);		
				if ($numrows2 <= 0) {
					echo "Cannot find record";
				} else {
					$row_counter2=0;
					while ($row_counter2 < $numrows2 && $row3 = pg_fetch_array($result2, $row_counter2,PGSQL_ASSOC)) {
						$currLang = $row3["lang"];
						if ($currLang == "en-us") {
							$titleEn = $row3["data"];	
						} else if ($currLang == "fr-fr") {
							$titleFrench = $row3["data"];
						} else if ($currLang == "ja") {
							$titleJapan = $row3["data"];
						} else if ($currLang == "ar") {
							$titleArabic = $row3["data"];
						} else if ($currLang == "es") {
							$titleSpanish = $row3["data"];
						} else if ($currLang == "de") {
							$titleDeutsch = $row3["data"];
						} else if ($currLang == "ra") {
							$titleRussia = $row3["data"];
						} else if ($currLang == "pt") {
							$titlePortuguese = $row3["data"];
						} else if ($currLang == "zh-cs") {
							$titleSChinese = $row3["data"];
						} else if ($currLang == "zh-ct") {
							$titleTChinese = $row3["data"];
						} else if ($currLang == "ko") {
							$titleKorean = $row3["data"];
						}
						$row_counter2++;
					}
				}
				$result3 = pg_exec($GLOBALS["link"], "select * from devics.locale where key='{$rootObj['description']}';");
				$numrows3 = pg_numrows($result3);
				if ($numrows3 <= 0) {
					echo "Cannot find record";
				} else {
					$row_counter3=0;
					while ($row_counter3 < $numrows3 && $row4 = pg_fetch_array($result3, $row_counter3,PGSQL_ASSOC)) {
						$currLang = $row4["lang"];
						if ($currLang == "en-us") {
							$descEn = $row4["data"];	
						} else if ($currLang == "fr-fr") {
							$descFrench = $row4["data"];
						} else if ($currLang == "ja") {
							$descJapan = $row4["data"];
						} else if ($currLang == "ar") {
							$descArabic = $row4["data"];
						} else if ($currLang == "es") {
							$descSpanish = $row4["data"];
						} else if ($currLang == "de") {
							$descDeutsch = $row4["data"];
						} else if ($currLang == "ra") {
							$descRussia = $row4["data"];
						} else if ($currLang == "pt") {
							$descPortuguese = $row4["data"];
						} else if ($currLang == "zh-cs") {
							$descSChinese = $row4["data"];
						} else if ($currLang == "zh-ct") {
							$descTChinese = $row4["data"];
						} else if ($currLang == "ko") {
							$descKorean = $row4["data"];
						}
						$row_counter3++;
					}
				}
				addChildOfCityAttractions($titleEn, $titleFrench, $titleJapan, $titleArabic, $titleSpanish, $titleDeutsch, $titleRussia, $titlePortuguese, $titleSChinese, $titleTChinese, $titleKorean, $descEn, $descFrench, $descJapan, $descArabic, $descSpanish, $descDeutsch, $descRussia, $descPortuguese, $descSChinese, $descTChinese, $descKorean, $position, $responseObj["msg"], $rootObj['image']);
				$row_counter11++;
			}
		}
	}
}

function addChildOfCityAttractions($titleEn, $titleFrench, $titleJapan, $titleArabic, $titleSpanish, $titleDeutsch, $titleRussia, $titlePortuguese, $titleSChinese, $titleTChinese, $titleKorean ,$descEn, $descFrench, $descJapan, $descArabic, $descSpanish, $descDeutsch, $descRussia, $descPortuguese, $descSChinese, $descTChinese, $descKorean, $position, $parentId, $imagePath) {
	$section="article";
    $data = array ('title_en' => $titleEn, 'title_fr' => $titleFrench, 'title_jp' => $titleJapan, 'title_es' => $titleSpanish, 'title_de' => $titleDeutsch, 'title_ru' => $titleRussia, 'title_pt' => $titlePortuguese, 'title_zh_cn' => $titleSChinese, 'title_zh_hk' => $titleTChinese, 'title_ko' => $titleKorean, 'desc_en' => $descEn, 'desc_fr' => $descFrench, 'desc_jp' => $descJapan, 'desc_ar' => $descArabic, 'desc_es' => $descSpanish, 'desc_de' => $descDeutsch, 'desc_ru' => $descRussia, 'desc_pt' => $descPortuguese, 'desc_zh_cn' => $descSChinese, 'desc_zh_hk' => $descTChinese, 'desc_ko' => $descKorean, 'type' => $section, 'parentId'=>$parentId, 'order'=>$position);

    $data = http_build_query($data);

    $context_options = array (
        'http' => array (
            'method' => 'POST',
            'header'=> "Content-type: application/x-www-form-urlencoded\r\n"
                . "Content-Length: " . strlen($data) . "\r\n",
            'content' => $data
        )
    );

    $context = stream_context_create($context_options);
    //$result123 = file_get_contents(THE_ROOT_PATH.'api/dm/createNewItemDM.php', false, $context);
	$fp = fopen(THE_ROOT_PATH.'api/dm/eventsandfestivals/createEventsAndFestivalsItems.php', 'r', false, $context);
    //If $result is FALSE, then the request has failed.
    if($fp === false){
        //If the request failed, throw an Exception containing
        //the error.
		echo "POST Failed!!!";
        $error = error_get_last();
        throw new Exception('POST request failed: ' . $error['message']);
    } else {
		$response = stream_get_contents($fp);
		$responseObj = json_decode($response,true);		
		if (!empty($imagePath)){
		    $typeImage="prefer";
			downloadImage($typeImage, $responseObj["msg"], $imagePath);
		}		
	}
}

function downloadImage($typeImage, $itemId, $imagePath){
	$imageName = substr($imagePath,7,strpos($imagePath,".")-7);
	$fullFileName = substr($imagePath,7,strlen($imagePath)-7);
    $urlbase = 'http://192.168.15.114/cmsppr/uploadtest/';
    $url = $urlbase.$fullFileName;
    //file_put_contents($img, file_get_contents($url));

    $b64image = base64_encode(file_get_contents($url));
	addImagesAndMaps($typeImage, $itemId, $b64image, $imageName, $fullFileName);
}

function addImagesAndMaps($typeImage, $itemId, $imageData, $imageName, $fullFileName) {
    $data = array ('type' => $typeImage, 'image' => $imageData,'itemId' => $itemId, 'fileName' => $imageName, 'fullFileName' => $fullFileName);

    $data = http_build_query($data);

    $context_options = array (
        'http' => array (
            'method' => 'POST',
            'header'=> "Content-type: application/x-www-form-urlencoded\r\n"
                . "Content-Length: " . strlen($data) . "\r\n",
            'content' => $data
        )
    );

    $context = stream_context_create($context_options);
    $fp = fopen(THE_ROOT_PATH.'api/dm/eventsandfestivals/createMediaAndMap.php', 'r', false, $context);
    //If $result is FALSE, then the request has failed.
    if($fp === false){
        //If the request failed, throw an Exception containing
        //the error.
		echo "POST Failed!!!";
        $error = error_get_last();
        throw new Exception('POST request failed: ' . $error['message']);
    } 	
}
