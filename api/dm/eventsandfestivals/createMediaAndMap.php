<?php

ini_set( "display_errors", true );
require("../../../config.php");

require("../../../php/inc.appvars.php");
require("../../../php/func_nx.php");

require("../../../php/lib/resize.class.php");

session_start();
//include("checkSession.php");

$itemId= isset($_REQUEST['itemId'])?$_REQUEST['itemId']:"";
$fileName= isset($_REQUEST['fileName'])?$_REQUEST['fileName']:"";
$photo_data= isset($_REQUEST['image'])?$_REQUEST['image']:"";
$fullFileName= isset($_REQUEST['fullFileName'])?$_REQUEST['fullFileName']:"";
$type= isset($_REQUEST['type'])?$_REQUEST['type']:"";

$isIcon = 0;

$extension="";
if (strpos($fullFileName, 'jpg') !== false) {
	$extension='j';
	$fileExtension = "jpg";
}
if (strpos($fullFileName, 'jpeg') !== false) {
	$extension='j';
	$fileExtension = "jpg";
}
if (strpos($fullFileName, 'png') !== false) {
	$extension='p';
	$fileExtension = "png";
}
if (strpos($fullFileName, 'pdf') !== false) {
	$extension='pdf';
	$fileExtension = "pdf";
}

if ( empty($itemId)){
    echo returnStatus(0, 'missing_item_id');
    exit;
}
else{
    $file_name_big = $fileName . '.' .$fileExtension;
    $file_name_big_image = $file_name_big;

    $file_name_medium = $fileName . '_m' .'.'. $fileExtension;
    $file_name_small = $fileName . '_s'.'.'.$fileExtension;
    // save origin photo or pdf
    if ( file_put_contents('../../../upload/'.$file_name_big, base64_decode($photo_data))) {
        if($fileExtension=='pdf') {
                try {
                    $im = new Imagick();

                    $im->setResolution(100, 100);
                    $text_name = '../../upload/'.$file_name_big.'[0]';
                    $im->readImage($text_name);
                    $im->setImageFormat('jpeg');
                    $file_name_big_image = $fileName . '.' .'jpg';
                    $file_name_medium = $fileName . '_m' .'.'.'jpg';
                    $file_name_small = $fileName . '_s' .'.'.'jpg';
                    $im->writeImage('../../upload/'.$file_name_big_image);
                    $im->clear();
                    $im->destroy();
                } catch(Exception $e) {
                    pprint_r($e);
                }

        }

        $middle_size_width = 1024;
        $small_size_width = 300;

        if($type != null && $type == 'icon'){
            $middle_size_width = 100;
            $small_size_width = 50;
        }
        try{
            resizeSavePhoto('../../../upload/'.$file_name_big_image , $middle_size_width ,'../../../upload/'.$file_name_medium);
            resizeSavePhoto('../../../upload/'.$file_name_big_image , $small_size_width ,'../../../upload/'.$file_name_small);
        } catch(Exception $e){
            pprint_r($e);
		}	
		$conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
		$conn->exec("set names utf8");
	
    if($type == "icon"){
        $prefer = 0;
        $isIcon = 1;
        $isThumb = 0;
    } else {
        $prefer = 1;
        $isIcon = 0;
        $isThumb = 0;		
	}
	
		$sql = "SELECT UUID() AS UUID";
		$st = $conn->prepare ( $sql );
		$st->execute();

		$list = array();

		while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
			$list[] = $row;
		}

		$uuid = $list[0]["UUID"];

		$sql = "INSERT INTO media (media.id,fileName,uploadTime,uploadBy,fileExt) VALUES (:id,:fileName, now(),:email,:extension)";
		$st = $conn->prepare ( $sql );

		$st->bindValue( ":id", $uuid, PDO::PARAM_STR );
		$st->bindValue( ":fileName", $fileName, PDO::PARAM_STR );
		$st->bindValue( ":email", "system", PDO::PARAM_STR );
		$st->bindValue( ":extension", $extension, PDO::PARAM_STR );
		$st->execute();
	

		$photoId = $uuid;

		$sql = "INSERT INTO mediaItemMap (mediaId,itemId,lastUpdateTime,lastUpdateBy,prefer, isIcon ) VALUES (:mediaId, :itemId, now(),:email, :prefer,:isIcon)";
		$st = $conn->prepare ( $sql );
		$st->bindValue( ":mediaId", $photoId.'', PDO::PARAM_STR );
		$st->bindValue( ":itemId", $itemId.'', PDO::PARAM_STR );
		$st->bindValue( ":prefer", $prefer, PDO::PARAM_INT );
		$st->bindValue( ":isIcon", $isIcon, PDO::PARAM_INT );
		$st->bindValue( ":email", "system", PDO::PARAM_STR );

		$st->execute();
	}

    echo returnStatus(1 , 'good');
}

function resizeSavePhoto($filename, $new_w, $out_filename){

    //echo "resize fire";
    $file = $filename;

    list($width , $height) = getimagesize($file);
    $ratio = $new_w / $width;
    //$new_w = $width * $ratio ;
    $new_h = $height * $ratio;


    $params = array(
        'constraint' => array('width' => $new_w, 'height' => $new_h)
    );
    img_resize($file, $out_filename, $params);
}
?>
