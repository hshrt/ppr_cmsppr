<?php

include("../../../php/func_nx.php");
ini_set( "display_errors", true );

//very important, to define which DB you want to migrate to
define( "THE_ROOT_PATH", "http://192.168.15.114/cmsppr/");

//this is DB login of the local postgres DB
$GLOBALS["link"] = pg_connect("dbname=postgres user=postgres password=esdesd");

$result = pg_exec($GLOBALS["link"], "SET client_encoding = 'UTF8';");
pg_set_client_encoding($GLOBALS["link"], "UTF8");

$id="9e22d257-e662-4be4-85d9-ee587629fef6";
$result = pg_exec($GLOBALS["link"], "select * from devics.content where parent_id='{$id}';");
$numrows = pg_numrows($result);

if (sizeof($numrows) <= 0) {
    echo "Cannot find record 1";
} else {
	$row_counter=0;
	while ($row_counter < $numrows && $row = pg_fetch_array($result, $row_counter,PGSQL_ASSOC)) {
		$rootObj = json_decode($row["data"], true);
		$selfId=$row['id'];
        $result1 = pg_exec($GLOBALS["link"], "select * from devics.locale where key='{$rootObj['title']}';");
        $numrows1 = pg_numrows($result);
		if ($numrows1 <= 0) {
			echo "Cannot find record 2";
		} else {
			$row_counter22=0;
			$titleEn="";
			$titleFrench="";
			$titleJapan="";
			$titleArabic="";
			$titleSpanish="";
			$titleDeutsch="";
			$titleRussia="";
			$titlePortuguese="";
			$titleSChinese="";
			$titleTChinese="";
			$titleKorean="";
			$descEn="";
			$descFrench="";
			$descJapan="";
			$descArabic="";
			$descSpanish="";
			$descDeutsch="";
			$descRussia="";
			$descPortuguese="";
			$descSChinese="";
			$descTChinese="";
			$descKorean="";	
			while ($row_counter22 < $numrows1 && $row1 = pg_fetch_array($result1, $row_counter22, PGSQL_ASSOC)) {
				$currLang = $row1["lang"];
				if ($currLang == "en-us") {
					$titleEn = $row1["data"];	
				} else if ($currLang == "fr-fr") {
					$titleFrench = $row1["data"];
				} else if ($currLang == "ja") {
					$titleJapan = $row1["data"];
				} else if ($currLang == "ar") {
					$titleArabic = $row1["data"];
				} else if ($currLang == "es") {
					$titleSpanish = $row1["data"];
				} else if ($currLang == "de") {
					$titleDeutsch = $row1["data"];
				} else if ($currLang == "ra") {
					$titleRussia = $row1["data"];
				} else if ($currLang == "pt") {
					$titlePortuguese = $row1["data"];
				} else if ($currLang == "zh-cs") {
					$titleSChinese = $row1["data"];
				} else if ($currLang == "zh-ct") {
					$titleTChinese = $row1["data"];
				} else if ($currLang == "ko") {
					$titleKorean = $row1["data"];
				}
				//echo $title123."=titleTitle<br>";
				$row_counter22++;
			}
			$result3 = pg_exec($GLOBALS["link"], "select * from devics.locale where key='{$rootObj['description']}';");
			$numrows3 = pg_numrows($result3);
			if ($numrows3 <= 0) {
				echo "Cannot find desc record";
			} else {
				$row_counter3=0;
				while ($row_counter3 < $numrows3 && $row4 = pg_fetch_array($result3, $row_counter3,PGSQL_ASSOC)) {
					$currLang = $row4["lang"];
					if ($currLang == "en-us") {
						$descEn = $row4["data"];	
					} else if ($currLang == "fr-fr") {
						$descFrench = $row4["data"];
					} else if ($currLang == "ja") {
						$descJapan = $row4["data"];
					} else if ($currLang == "ar") {
						$descArabic = $row4["data"];
					} else if ($currLang == "es") {
						$descSpanish = $row4["data"];
					} else if ($currLang == "de") {
						$descDeutsch = $row4["data"];
					} else if ($currLang == "ra") {
						$descRussia = $row4["data"];
					} else if ($currLang == "pt") {
						$descPortuguese = $row4["data"];
					} else if ($currLang == "zh-cs") {
						$descSChinese = $row4["data"];
					} else if ($currLang == "zh-ct") {
						$descTChinese = $row4["data"];
					} else if ($currLang == "ko") {
						$descKorean = $row4["data"];
					}
					$row_counter3++;
				}
			}
			addHotelRestaurants($titleEn, $titleFrench, $titleJapan, $titleArabic, $titleSpanish, $titleDeutsch, $titleRussia, $titlePortuguese, $titleSChinese, $titleTChinese, $titleKorean, $descEn, $descFrench, $descJapan, $descArabic, $descSpanish, $descDeutsch, $descRussia, $descPortuguese, $descSChinese, $descTChinese, $descKorean, $selfId, $rootObj['image']);
		}
        $row_counter++;
	}
}

function addHotelRestaurants($titleEn, $titleFrench, $titleJapan, $titleArabic, $titleSpanish, $titleDeutsch, $titleRussia, $titlePortuguese, $titleSChinese, $titleTChinese, $titleKorean,$descEn, $descFrench, $descJapan, $descArabic, $descSpanish, $descDeutsch, $descRussia, $descPortuguese, $descSChinese, $descTChinese, $descKorean, $selfId, $imagePath) {
	$section="Spa/Restaurant";
    $data = array ('title_en' => $titleEn, 'title_fr' => $titleFrench, 'title_jp' => $titleJapan, 'title_ar' => $titleArabic, 'title_es' => $titleSpanish, 'title_de' => $titleDeutsch, 'title_ru' => $titleRussia, 'title_pt' => $titlePortuguese, 'title_zh_cn' => $titleSChinese, 'title_zh_hk' => $titleTChinese, 'title_ko' => $titleKorean, 'desc_en' => $descEn, 'desc_fr' => $descFrench, 'desc_jp' => $descJapan, 'desc_ar' => $descArabic, 'desc_es' => $descSpanish, 'desc_de' => $descDeutsch, 'desc_ru' => $descRussia, 'desc_pt' => $descPortuguese, 'desc_zh_cn' => $descSChinese, 'desc_zh_hk' => $descTChinese, 'desc_ko' => $descKorean, 'type' => $section, 'parentId'=>'1');

    $data = http_build_query($data);

    $context_options = array (
        'http' => array (
            'method' => 'POST',
            'header'=> "Content-type: application/x-www-form-urlencoded\r\n"
                . "Content-Length: " . strlen($data) . "\r\n",
            'content' => $data
        )
    );

    $context = stream_context_create($context_options);
    //$result123 = file_get_contents(THE_ROOT_PATH.'api/dm/createNewItemDM.php', false, $context);
	$fp = fopen(THE_ROOT_PATH.'api/dm/hotelrestaurants/createHotelRestaurantsItems.php', 'r', false, $context);
    //If $result is FALSE, then the request has failed.
    if($fp === false){
        //If the request failed, throw an Exception containing
        //the error.
		echo "POST Failed!!!";
        $error = error_get_last();
        throw new Exception('POST request failed: ' . $error['message']);
    } else {
		$response = stream_get_contents($fp);
		$responseObj = json_decode($response,true);
		if (!empty($imagePath)){
		    $typeImage="prefer";
			downloadImage($typeImage, $responseObj["msg"], $imagePath);
		}
	}
}

function downloadImage($typeImage, $itemId, $imagePath){
	$imageName = substr($imagePath,7,strpos($imagePath,".")-7);
	$fullFileName = substr($imagePath,7,strlen($imagePath)-7);
    $urlbase = 'http://192.168.15.114/cmsppr/uploadtest/';
    $url = $urlbase.$fullFileName;
    //file_put_contents($img, file_get_contents($url));

    $b64image = base64_encode(file_get_contents($url));
	addImagesAndMaps($typeImage, $itemId, $b64image, $imageName, $fullFileName);
}

function addImagesAndMaps($typeImage, $itemId, $imageData, $imageName, $fullFileName) {
    $data = array ('type' => $typeImage, 'image' => $imageData,'itemId' => $itemId, 'fileName' => $imageName, 'fullFileName' => $fullFileName);

    $data = http_build_query($data);

    $context_options = array (
        'http' => array (
            'method' => 'POST',
            'header'=> "Content-type: application/x-www-form-urlencoded\r\n"
                . "Content-Length: " . strlen($data) . "\r\n",
            'content' => $data
        )
    );

    $context = stream_context_create($context_options);
    $fp = fopen(THE_ROOT_PATH.'api/dm/hotelrestaurants/createMediaAndMap.php', 'r', false, $context);
    //If $result is FALSE, then the request has failed.
    if($fp === false){
        //If the request failed, throw an Exception containing
        //the error.
		echo "POST Failed!!!";
        $error = error_get_last();
        throw new Exception('POST request failed: ' . $error['message']);
    }
}
