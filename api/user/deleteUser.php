<?php

ini_set( "display_errors", true );
require("../../config.php");
require("../../php/inc.appvars.php");

session_start();
include("../checkSession.php");

$userId = isset($_POST['userId'])?$_POST['userId']:null;


if (empty($userId)){
    echo returnStatus(0, 'missing userId');
    exit;
}

$conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
$conn->exec("set names utf8");



$sql = "DELETE FROM user where id = :id";

$st = $conn->prepare ( $sql );

$st->bindValue( ":id", $userId, PDO::PARAM_STR );

$st->execute();

$list = array();

while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
    $list[] = $row;
    //echo json_encode($row);
}


if($st->rowCount() > 0)
    echo returnStatus(1 , 'delete ok!');
else
    echo returnStatus(0 , 'delete fail! ');

$conn = null;

?>
