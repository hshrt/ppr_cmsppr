<!DOCTYPE html>
<html lang="en">
<head>
    <title>Print Message</title>

    <link rel="shortcut icon" href="../../images/favicon.ico">
    <link rel="apple-touch-icon" href="../../images/favicon.ico">

    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

    <link rel="stylesheet" type="text/css" href="../../css/main.css" />

    <link rel="stylesheet" type="text/css" href="../../css/simplePagination.css" />

    <link rel="stylesheet" type="text/css" href="../../3rdparty/timepicker/jquery.timepicker.min.css" />

    <!-- Theme style -->
    <link href="../../css/dist/css/AdminLTE.css" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the
    load. -->
    <link href="../../css/dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />
    <!--<link href="css/extra.css" rel="stylesheet" type="text/css" />-->

    <!--image crop Library-->
    <link href="../../3rdparty/cropper/cropper.css" rel="stylesheet" type="text/css" />


    <link href="http://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">

    <!-- Bootstrap 3.3.2 -->
    <link href="../../3rdparty/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font- awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />



    <!--<script src="//ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>-->
    <script src="../../3rdparty/jquery-2.1.1.min.js"></script>
</head>

<?php $msg_id= $_REQUEST["msgId"];?>

<div class="a4Container">
    <div id="header">
        <img id="pen_logo" src="../../images/peninsula.jpg" ?="">
    </div>
    <div id="content">

        <div class="container-fluid" id="title"></div>
        <div class="container-fluid">

                <div class="firstCol">Message For</div>
                <div class="quote">:</div>
                <div class="thirdCol" id="name"></div>


                <div class="firstCol">Room Number</div>
                <div class="quote">:</div>
                <div class="thirdCol" id="room"></div>


                <div class="firstCol">Date and Time</div>
                <div class="quote">:</div>
                <div class="thirdCol" id="timeBox"></div>

            <!--<div class="row">
                <div class="col-md-2">Message Ref</div>
                <div class="quote">:</div>
                <div class="col-md-4">Ms Jordan</div>
            </div>-->
        </div>
        <div class="container-fluid">
            <div id="des">
            </div>
        </div>
    </div>

    <div class="footer">The Peninsula Paris,
        19 Avenue Kléber Paris, France, 75116<br>
        Telephone: +33 1 5812 2888
        E-mail: ppr@peninsula.com


    </div>
</div>

<script>
    var App = {};
    $.ajax({
        url : "../msg/getMessage.php",
        method : "POST",
        dataType: "json",
        data : {itemId:"<?php echo($msg_id);?>" }
    }).success(function(json){
        console.log("firstItemList = " + json);
        App.currentItem = json.data[0];

        //get message detail
        $.ajax({
            url : "../msg/getMessage.php",
            method : "POST",
            dataType: "json",
            data : {getActive:"true"}
        }).success(function(json){
            console.log("all message" + json.data[0]);


            for (var x = 0; x< json.data.length;x++){
                console.log(json.data[x].name_list);
                if (json.data[x].id == App.currentItem.id){
                    $("#name").text(json.data[x].name_list);
                    $("#room").text(json.data[x].room_list);
                    $("#timeBox").text(json.data[x].lastUpdate);
                }
            }

        }).error(function(d){
            console.log('error');
            console.log(d);
        });

        //get language info for item Title
        $.ajax({
            url : "../getLangForKey.php",
            method : "POST",
            dataType: "json",
            data : {id:App.currentItem.subjectId, forMsg:1}
        }).success(function(json){
            console.log(json.data[0]);


            self.titleLangModel = json.data[0];

            $("#title").html(self.titleLangModel.en);

            //console.log(that.titleLangModel.zh_hk);

        }).error(function(d){
            console.log('error');
            console.log(d);
        });

        //get language info for item Description
        $.ajax({
            url : "../getLangForKey.php",
            method : "POST",
            dataType: "json",
            data : {id:App.currentItem.descriptionId, forMsg:1 }
        }).success(function(json){
            console.log(json.data[0]);

            self.desLangModel = json.data[0];


            console.log("original = " + self.desLangModel.en);

            self.desLangModel.en = self.desLangModel.en.replace(/\n/g, "<br />");
            $("#des").html(self.desLangModel.en);

        }).error(function(d){
            console.log('error');
            console.log(d);
        });


    }).error(function(d){
        console.log('error');
        console.log(d);
    });

</script>
