<?php
ini_set( "display_errors", true );
date_default_timezone_set( "Europe/Paris" );  // http://www.php.net/manual/en/timezones.php

//CMS server login info
define("DB_NAME","BSPPPR");
define( "DB_DSN", "mysql:host=localhost;dbname=".DB_NAME.";charset=utf8" );
define( "DB_USERNAME", "root" );
define( "DB_PASSWORD", "i;like;Ppr@28148050");

//BPC server login info
define( "DB_DSN_BPC", "mysql:host=172.30.16.107;dbname=BPC_local;charset=utf8" );
define( "DB_USERNAME_BPC", "marco" );
define( "DB_PASSWORD_BPC", "marco.esd");

//Weather server login info
define( "DB_DSN_WEATHER", "mysql:host=phkhgc.hsh-grt.com;dbname=Weather;charset=utf8" );
define( "DB_USERNAME_WEATHER", "marco");
define( "DB_PASSWORD_WEATHER", "marco");

//Video Injection server URL
define("VIDEO_INJECTION_URL", '192.168.5.162:8000/');
define("VIDEO_INJECTION_USER", 'app');
define("VIDEO_INJECTION_PASSWORD", '1234');
//http://172.30.6.162:8000/Video_Injection/default/

//the base folder which contain all CMS source code
define("BASE_FOLDER", "cmsppr");
//the name of the location of the hotel shown in the URL
define("HOTEL_LOCATION", "paris");
//this is either "production" or "staging"
define("SERVER_TYPE", "production");
//the header text shown in the browser
define("HEADER_TEXT", "HSH PPR CMS R&T Test");
//the version number
define("VERSION_NUM", "1.2.10");

//only useful for data Migration API
define( "ROOT_PATH", "http://192.168.15.114/cmsppr/" );

define("OK",200);
define("Created",201);
define("No_Content",204);
define("Not_Modified",304);
define("Bad_Request",400);
define("Unauthorized",401);
define("Forbidden",403);
define("Not_Found",404);
define("Method_Not_Allowed",405);
define("Gone",410);
define("Unsupported_Media_Type",415);
define("Unprocessable_Entity",422);
define("Too_Many_Requests",429);

define("Invalid_input",501);
define("Session_timeout",502);

function handleException( $exception ) {
  echo "Sorry, a problem occurred. Please try later.";
  error_log( $exception->getMessage() );
}

set_exception_handler( 'handleException' );
?>
