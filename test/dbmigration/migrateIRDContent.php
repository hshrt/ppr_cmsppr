<?php

include("../../php/func_nx.php");
ini_set( "display_errors", true );

//very important, to define which DB you want to migrate to
define( "THE_ROOT_PATH", "http://192.168.11.61/cmsppr_new/");

//this is DB login of the local postgres DB
$GLOBALS["link"] = pg_connect("dbname=postgres user=postgres password=postgres");

$result = pg_exec($GLOBALS["link"], "SET client_encoding = 'UTF8';");
pg_set_client_encoding($GLOBALS["link"], "UTF8");

$result = pg_exec($GLOBALS["link"], "select * from devics.content;");
$numrows = pg_numrows($result);

if (sizeof($numrows) <= 0) {
    echo "Cannot find record";
} else {
	$row_counter=0;
    while ($row_counter < $numrows && $row = pg_fetch_array($result, $row_counter,PGSQL_ASSOC)) {
		$rootObj = json_decode($row["data"], true);
		$schmea = $rootObj['_schema'];
		$titles = htmlspecialchars($rootObj['title']);
		$descs = htmlspecialchars($rootObj['description']);
		if($schmea == "StoreMenu") {
			echo $schmea."=schema<br>";
			echo $titles."=title<br>";
			echo $descs."=description<br>";
            $result1 = pg_exec($GLOBALS["link"], "select * from devics.locale where lang='en-us' and key='{$rootObj['title']}';");
            $numrows1 = pg_numrows($result1);
			if ($numrows1 <= 0) {
				echo "Cannot find record";
			} else {
				$row_counter1=0;
				$parentId="363e00c2-fc18-11e8-932a-0050569631b5";
				while ($row_counter1 < $numrows1 && $row1 = pg_fetch_array($result1, $row_counter1,PGSQL_ASSOC)) {
					$email= $row1["data"];
					//echo $email."=titleTitle<br>";
					addIRDSection($email,"section",$parentId);
					$row_counter1++;
				}
			}
		}
		$row_counter++;
	}
    //foreach ($GLOBALS["descriptions"] as $menuId) {
        //$menu = $GLOBALS["descriptions"][$menuId];
	//	echo $menuId;
	//}
}

function addIRDSection($title, $type, $parentId){
    //setup input parameter

    echo "addDictionaryKey fire first!<br>";
    $data = array ('title' => $title, 'type' => $type, 'parentId' => $parentId);

    $data = http_build_query($data);

    $context_options = array (
        'http' => array (
            'method' => 'POST',
            'header'=> "Content-type: application/x-www-form-urlencoded\r\n"
                . "Content-Length: " . strlen($data) . "\r\n",
            'content' => $data
        )
    );

    $context = stream_context_create($context_options);
    $result123 = file_get_contents(THE_ROOT_PATH.'api/createNewItem.php', false, $context);
    //If $result is FALSE, then the request has failed.
    if($result123 === false){
        //If the request failed, throw an Exception containing
        //the error.
        $error = error_get_last();
        throw new Exception('POST request failed: ' . $error['message']);
    }
    //If everything went OK, return the response.
    return $result123;

}



?>
