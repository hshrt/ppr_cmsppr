<!-- popup box setup -->
<div class='popup_box_container scroll'>

    <div class='closeBtnPrint' id='closeBtn'></div>

    <div class='print_container'>
        <div class="a4Container">
            <div id="header">
                <img id='pen_logo' src="images/PBH_logo.jpg"?>
            </div>
            <div id="content">
                <div id=title></div>
                <div id="time"></div>
                <img id='item_pic' src="">
                <div id="des"></div>


            </div>
            <div class="footer">THE PENINSULA BEVERLY HILLS
                9882 South Santa Monica Boulevard
                Beverly Hills, CA, 90212, USA</br>
                Toll-free: 1 800 462 7899 (US & Canada)
                Tel: +1 310 551 2888
                Fax: +1 310 788 2319
                E-mail: pbh@peninsula.com

            </div>
        </div>


    </div>
</div>