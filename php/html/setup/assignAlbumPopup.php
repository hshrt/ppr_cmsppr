<!-- popup box setup -->
<div class='popup_box_container'>
    <div class='popup_box'>
        <div id="head">
            <h1 id="create_box_title"></h1>
            <div class='closeBtn' id='closeBtn'></div>
        </div>

        <div id="middle">


            <h4 id="field1_label"></h4>
            <div id="assignAlbumSection">
                <label>Album:</label></br>
                <select class="form-control" id='albumSelectionBox' name='album'>
                    <option value=''>all</option>
                </select>
            </div>


        </div>

        <div id="foot">
            <div class="actions">

                <div style='float:right' class='submit round_btn btn bg-olive' id="saveCreateBtn">Save</div>
            </div>
        </div>
    </div>
</div>