<!-- popup box setup -->
<div class='popup_box_container'>
    <div class='popup_box'>
        <h1 id="create_box_title"></h1>
        <div class='closeBtn' id='closeBtn'></div>

        <div class='half_width_container half_width_container_popup' id= 'container_salutation' >
            <label for='title' >First Name</label>
            <input id='firstNameInput' type='text' name='title' tabindex='1' data-type='text' value='' autofocus=''
                   maxlength="40">
        </div>
        <div class='half_width_container half_width_container_popup' id= 'container_firstName'>
            <label for='title' >Last Name</label>
            <input id='lastNameInput' type='text' name='title' tabindex='1' data-type='text' value='' autofocus='' maxlength="40">
        </div>
        <div class='half_width_container half_width_container_popup' id= 'container_lastName'>
            <label for='title' >Email Address</label>
            <input id='emailInput' type='text' name='title' tabindex='1' data-type='text' value='' autofocus=''
                   maxlength="50">
        </div>
        <div class='half_width_container half_width_container_popup' id= 'container_roomNum'>
            <label for='title' >Role</label>
            <select id="roleInput" name="type">
                <option value="admin">Admin</option>
                <!--<option value="Information with top sub menu">Information (with top sub menu)</option>-->
                <option value="editor">Editor</option>
                <option value="operation">Operation</option>
            </select>
        </div>



        <div class='half_width_container half_width_container_popup' id= 'container_checkIn'>
                <label for='title' id='checkInDateLabel'>Password</label>
                <input id='passwordInput' class='form-control' type="password" name='title' tabindex='1' data-type='text' value='' autofocus='' maxlength="15">

        </div>

        <div class='half_width_container half_width_container_popup' id= 'container_checkInTime'>
            <label for='title' id='checkInTimeLabel'>Confirm Password</label>
            <input id='confirmPasswordInput' class='form-control' type="password" name='title' tabindex='1' data-type='text' value=''
                   autofocus='' maxlength="15">
        </div>



        <div class="actions">
            <div style='float:right' class='submit round_btn btn btn bg-olive' id="createBtn">Create</div>
        </div>
    </div>
</div>