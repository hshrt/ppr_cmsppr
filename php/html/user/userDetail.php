<div class="box detail_container" id="item_list_boxer">
    <div class="box-header" style="padding-top:0px">

        <h3 style="float:left" id="itemName">Library</h3>

    </div>
    <div class="box-body">

        <div class='half_width_container half_width_container_popup' id= 'container_firstName'>
            <label for='title' id='firstNameLabel'>First Name</label>
            <input id='firstNameInput' class='form-control' type='text' name='title' tabindex='1' data-type='text' value='' autofocus=''>
        </div>
        <div class='half_width_container half_width_container_popup' id= 'container_lastName'>
            <label for='title' id='lastNameLabel'>Last Name</label>
            <input id='lastNameInput' class='form-control' type='text' name='title' tabindex='1' data-type='text' value='' autofocus=''>
        </div>
        <div class='half_width_container half_width_container_popup' id= 'container_email'>
            <label for='title' >Email</label>
            <input id='emailInput' class='form-control' type='text' name='title' tabindex='1' data-type='text' value='' autofocus=''>
        </div>

        <div class='half_width_container half_width_container_popup' id= 'container_role'>
            <label for='title' id='RoleLabel'>Role</label>
            <select id="roleInput" class='form-control' name="type">
                <option value="admin">Admin</option>
                <!--<option value="Information with top sub menu">Information (with top sub menu)</option>-->
                <option value="editor">Editor</option>
            </select>
        </div>


    </div><!-- /.box-body -->
    <div class="box-footer">
        <div class='save_cancel_container'>
            <div class='round_btn btn bg-maroon' id='delete_btn'>Delete Section</div>
            <div class='round_btn btn bg-olive' id='save_btn'>Save Section</div>
        </div>
    </div><!-- /.box-footer-->
</div>